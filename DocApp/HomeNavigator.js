import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import {Image } from 'react-native';
import { createAppContainer} from "react-navigation";
import {createBottomTabNavigator} from 'react-navigation-tabs';
import Home from './Home/Home';
import DoctorDetail from './Home/DoctorDetail';
import ChooseConsultation from './Home/ChooseConsultation';
import Prescription from './Home/Prescription';
import Profile from './Home/Profile';
import PrescriptionDetail from './Home/PrescriptionDetail';
import DoctorList from './Home/DoctorList';
import PopularCategories from './Home/PopularCategories';
import Appointment from './Home/Appointment';
// import VideoHome from './VideoCall/VideoHome';
// import Video from './VideoCall/Video';
const HomeTab = createStackNavigator({
  Home: Home ,
  DoctorDetail : DoctorDetail,
  ChooseConsultation : ChooseConsultation,
  DoctorList : DoctorList,
  PopularCategories : PopularCategories,
  Appointment : Appointment,
  // Video  : Video,
},
{
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: '#000000',
    },
    headerTintColor: '#000000',
    title: 'Home',
   
  },
}
);



const HomeTab1 = createStackNavigator({
  Home: Home ,
  DoctorDetail : DoctorDetail,
  ChooseConsultation : ChooseConsultation,
  Prescription : Prescription,
  DoctorList : DoctorList,
  PopularCategories : PopularCategories,
  Appointment : Appointment,
},
{
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: '#000000',
    },
    headerTintColor: '#000000',
    title: 'Home',
   
  },
}
);

const AssetsTab = createStackNavigator({
  Prescription: Prescription ,
  PrescriptionDetail : PrescriptionDetail,
},
{
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: '#000000',
    },
    headerTintColor: '#000000',
    title: 'Search',
   
  },
}
);


const AlertsTab = createStackNavigator({
  Profile: Profile ,
},
{
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: '#000000',
    },
    headerTintColor: '#000000',
    title: 'MyOrders',
   
  },
}
);


const PropertiesNavigator = createBottomTabNavigator(
  {
    Home: HomeTab1 ,
    Prescriptions: AssetsTab ,
    Profile: AlertsTab ,
   
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        if (routeName === 'Home') {
          return (
            <Image
              source={ require('./assets/home.png') }
              style={{ width: 20, height: 20 }} />
              
          );

        } 
        if (routeName === 'Prescriptions') {
          return (
            <Image
             source={ require('./assets/google-docs.png') }
              style={{ width: 20, height: 20,tintColor:'#000000' }} />
          );
        }
        else if (routeName === 'Profile') {
          return (
            <Image
              source={ require('./assets/user.png') }
              style={{ width: 20, height: 20,tintColor:'#000000' }} />
          );
        } else {
          return (
            <Image
             source={ require('./assets/dcsmall.png') }
              style={{ width: 20, height: 20,tintColor:'#000000' }} />
          );
        }
      },
    }),
    tabBarOptions: {
      activeTintColor: '#000000',
      inactiveTintColor: '#666666',
      
      style: {
      
      
      backgroundColor: '#ffffff',
      },
    },
  }
);
 
 
export default createAppContainer(PropertiesNavigator);