import { StyleSheet, Platform,Dimensions } from "react-native";


export default StyleSheet.create({
  Maincontainer: {
    
    // alignItems: 'center',
   
    // justifyContent: 'center',
    flex: 1,
    
  },
  horiz:{
  

    flexDirection : "row",
    marginTop:10,
    alignContent:"center",
    alignItems:"center",
    alignSelf:"center"
   },
   
  containerMainLogin:{
    margin:20,
    paddingBottom:30,
    marginTop:10,
    padding:20,
    borderRadius:5
    // backgroundColor:"#ffffff"
  },
  HeadingText : {
    color : "#000000",
    fontSize : 18,
    fontWeight : "bold",
    marginTop : 10,

  },
  textInputContainer: {
    marginBottom: 10,
    width:5,
  },
  container: {
   
    height:80,
    width:80,
    padding:5,
    alignContent:"center",
    alignItems:"center",
    alignSelf:"center",
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    // alignItems: 'center',
    justifyContent: 'center',
  
  },
  inoutBoxPhone: {
    height:40,
    width:250,
    paddingTop:10,
    paddingBottom:10,
    paddingHorizontal:10,
    backgroundColor:'#fff',
    borderBottomWidth : 0.5,
    

  },
  forgot : {
    alignItems : 'flex-end',
    justifyContent : 'flex-end',
  },
  inputEmail: {
    height:40,
    paddingTop:10,
    paddingBottom:10,
    paddingHorizontal:10,
    backgroundColor:'#fff',
    marginTop:50
  },
  inputPassword: {
    height:40,
    paddingTop:10,
    paddingBottom:10,
    paddingHorizontal:10,
    backgroundColor:'#fff',
    marginBottom:10,
    marginTop:10
  },
  welcomeText:{
     alignItems: 'center',
    justifyContent: 'center',
    fontSize : 25,
    fontWeight : "bold",
    alignSelf: 'stretch',
    textAlign: 'center',
    color : '#00B7A5',
    paddingHorizontal:10
  },
  horizontalContainer1:{

    alignContent:'center',
    alignItems: 'center',
    justifyContent: 'center',
   flexDirection : "row"
  },

  horizontalContainer:{


    marginTop : 10,
    alignContent:"flex-start",
    alignItems:"flex-start",
    alignSelf:"flex-start",
   flexDirection : "row"
  },
  welcomeText1:{
 
   fontSize : 25,
   fontWeight : "bold",

   flex:1,
   color : '#000000',
   paddingTop : 50,
   paddingHorizontal:10
 },
 welcomeText2:{
 
  fontSize : 35,
  fontWeight : "bold",

  color : '#4FA647',
  paddingTop : 50,
  paddingHorizontal:10
},


  subText:{
    marginBottom:40,
    alignSelf: 'stretch',
    fontSize:16,
    textAlign: 'center',
    paddingHorizontal:10,
    color : '#2E2E2E'
  
  },
  subText1:{
    alignContent : "flex-start",
    marginBottom:5,
    marginTop: 10,
    fontSize:16,
    color : '#101010',
    paddingHorizontal:10
  },
  ButtonContainer:{
    alignItems:'center',
    alignContent:"center",
    alignSelf:"center",
    marginTop : 40,
    padding:25,
    width:100,
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor:'#4FA647',  
    paddingTop:15,
    paddingBottom:15,
    paddingHorizontal:10,
    marginBottom:10,
    borderRadius:25
  },
  setFontSize: {
   
    fontSize : 18,
   
  },
  inputBoxEmail:{
    alignSelf: 'stretch',
    height:40,
    paddingTop:5,
    paddingBottom:5,
    paddingHorizontal:10,
    marginTop:15,
    fontSize:16,
    borderWidth: 1,
    borderColor : '#eee',
    backgroundColor:'#fff',
    borderRadius:1
  },
  inputBoxPassword:{
    alignSelf: 'stretch',
    height:40,
    paddingTop:5,
    paddingBottom:5,
    paddingHorizontal:10,
    marginTop:10,
    fontSize:16,
    borderWidth: 1,
    borderColor : '#eee',
    backgroundColor:'#fff',
    borderRadius:1
  },
  imgmenu:{

    height : 250,
    width : '100%',
    marginTop:25,
  },
  header:{
    alignSelf: 'stretch',
    height : 100,
    backgroundColor : '#00B7A5',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 2,
  },
  inputIcon:{
    width:25,
    height:25,
    marginLeft:15,
    marginRight:10,
    justifyContent: 'center'
  },
  inputContainer: {
    // borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius:30,
    // borderBottomWidth: 1,
    width:'100%',
    height:45,
    marginBottom:10,
    flexDirection: 'row',
    alignItems:'center'
},
  setColorRed : {
    color: '#fff'
  },
  width_proportion : {
    width : '100%',
    height : '50%'
  }
,
Homecontainer: {
    
  // alignItems: 'center',
  backgroundColor:"#ffffff",
  // justifyContent: 'center',
  flex: 1,
  
},
appBarStyle: {
  backgroundColor:"#3E7C77",
  
},



subText:{
  marginBottom:40,
  alignSelf: 'stretch',
  fontSize:16,
  textAlign: 'center',
  paddingHorizontal:10,
  color : '#2E2E2E'

},
subText1:{
  alignContent : "flex-start",
  marginBottom:5,
  marginTop: 10,
  fontSize:16,
  color : '#101010',
  paddingHorizontal:10
},
ButtonContainer:{
  alignItems:'center',
  alignContent:"center",
  alignSelf:"center",
  marginTop : 40,
  padding:25,
  width:100,
  flexDirection: 'row',
  justifyContent: 'center',
  backgroundColor:'#4FA647',  
  paddingTop:15,
  paddingBottom:15,
  paddingHorizontal:10,
  marginBottom:10,
  borderRadius:25
},
setFontSize: {
 
  fontSize : 18,
 
},
inputBoxEmail:{
  alignSelf: 'stretch',
  height:40,
  paddingTop:5,
  paddingBottom:5,
  paddingHorizontal:10,
  marginTop:15,
  fontSize:16,
  borderWidth: 1,
  borderColor : '#eee',
  backgroundColor:'#fff',
  borderRadius:1
},
inputBoxPassword:{
  alignSelf: 'stretch',
  height:40,
  paddingTop:5,
  paddingBottom:5,
  paddingHorizontal:10,
  marginTop:10,
  fontSize:16,
  borderWidth: 1,
  borderColor : '#eee',
  backgroundColor:'#fff',
  borderRadius:1
},
bottomButton:{
  position: 'absolute',
  bottom:0,
  width:'100%',
  alignContent:"center",
  alignItems:"center",
  alignSelf:"center",
  height:50,
  padding:15,
  backgroundColor:"#2874f0"
},
menuImageStyle:{
  height:25,width:25,alignContent:"center",alignItems:"center",alignSelf:"center",tintColor:"#ffffff"
},
homePageLogoImageStyle:{
  height:50,width:50,alignContent:"center",alignItems:"center",alignSelf:"center",tintColor:"#ffffff"
},
cartImageStyle:{
  height:20,width:20,alignContent:"center",alignItems:"center",alignSelf:"center",tintColor:"#ffffff",marginLeft:10
},
backButtonStyle:{
  height:25,width:25,alignContent:"center",alignItems:"center",alignSelf:"center",tintColor:"#ffffff"
},
flatListImageStyle:{
  height:40,width:40,alignContent:"center",alignItems:"center",alignSelf:"center"

},
profileTextViewStyle:{
  fontSize:20,color:"#000000",alignContent:"flex-start",alignItems:"flex-start",alignSelf:"flex-start",
},
profileOptionTVStyle:{
  fontSize:20,color:"#000000",alignContent:"center",alignItems:"center",alignSelf:"center",width:"100%",
},
profileOptionViewStyle:{
  flexDirection:"row",margin:15,borderColor:'#eeeeee',
  borderWidth: 1,
  borderRadius:1,
  backgroundColor:'#ffffff',
  color:"#666",padding:15,margin:5
},
profileOptionsTextView:{

},
mapStyle: {
  width: Dimensions.get('window').width,
  height:"30%",
},
MapViewAddress:{
  position: 'absolute',
  bottom:0,
  width:'100%',
  alignContent:"center",
  alignItems:"center",
  alignSelf:"center",
  height:'50%',
  padding:15,
  backgroundColor:"#ffffff"
},
timeSelectionViewStyle:{
  borderWidth:1,borderColor:"#eeeeee",borderRadius:5,margin:10,backgroundColor:"#eeeeee",
  width:100,alignContent:"center",alignItems:"center",alignSelf:"center"
},

});
