import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from "react-navigation";
import Home from './Home/Home';
import Login from './Intro/Login';
import SignUp from './Intro/SignUp';
import DoctorDetail from './Home/DoctorDetail';
import ChooseConsultation from './Home/ChooseConsultation';
import Introduction from './Intro/Introduction';
import Dashbord from './Home/Dashbord';
import DoctorDashboard from './Doctor/DoctorDashboard';


const AppNavigator = createStackNavigator({
  Introduction : {screen : Introduction},
  Login : {screen: Login},

  Home : { screen : Home}, 
  SignUp : {screen : SignUp},
  DoctorDetail : {screen : DoctorDetail},
  ChooseConsultation : {screen : ChooseConsultation},
  Dashbord : {screen : Dashbord},

  DoctorDashboard : {screen : DoctorDashboard},
 
});

const App = createAppContainer(AppNavigator);
export default App;