import React from 'react';
import { Alert,Image,StyleSheet, Text, View, TextInput,TouchableOpacity,FlatList,ScrollView, KeyboardAvoidingView,AsyncStorage} from 'react-native';
import { ToastAndroid } from "react-native";

import { StackActions, NavigationActions } from 'react-navigation';
const width_proportion = '100%';
import axios from 'axios';
import Url from '../Utils/config';
var AuthStr='';

export default class DoctorDetail extends React.Component{
  static navigationOptions = {
    header: null ,

  };


 

  constructor(props) {
    super(props);
    this.state = {
     docName:'',
     docExperin:'',
     exp:'',
     call:'',
     video:'',
  
    }
  }

  componentDidMount(){

  this.getDocDetails();
  }
  
  async getDocDetails(){
    self=this;
    await AsyncStorage.getItem('token').then((token)=>
    {
     AuthStr = 'Bearer '+token; 
     console.log(AuthStr)
    axios.get(Url.BaseUrl + "doctor/"+this.props.navigation.getParam('id'),{ headers: { auth: AuthStr } })
   
  .then(function (responseJson) {
        console.log("DocDetailResp"+JSON.stringify(responseJson.data));
        self.setState({docName:responseJson.data.doctorData.name});
        self.setState({docExperin:responseJson.data.doctorData.expertIn});
        self.setState({exp:responseJson.data.doctorData.experience});
        self.setState({doctor_id:responseJson.data.doctorData._id});
        if(responseJson.data.doctorData.sessionFee){
          self.setState({call:responseJson.data.doctorData.sessionFee.call})
          self.setState({video:responseJson.data.doctorData.sessionFee.video})
        }
      
      })
      .catch((error) => {
        console.error(error);
      });
    }
    )
  }
  goback(){
    this.props.navigation.navigate("Home");
  }


  consult(){
    
    AsyncStorage.setItem('doctor_id',this.state.doctor_id).then(()=>
    this.props.navigation.navigate("ChooseConsultation")
    )
   
  }

  
    render(){
      const {navigate} = this.props.navigation;
    return (
      <ScrollView>
     <View style={styles.Maincontainer}>
      <View style={{flexDirection:"row",alignContent:"center",alignSelf:"center",alignItems:"center",marginTop:10}}>
      <TouchableOpacity onPress={this.goback.bind(this)}><Image resizeMode="contain" style={{height:35,width:35}} source={require('../assets/left-arrow.png')} /></TouchableOpacity>
      <Text style={{fontSize:18,fontWeight:"bold",flex:1,marginLeft:20}}>Doctor Details</Text>
      </View>

      <View style={{margin:5,marginTop:10,borderColor:"#bbdefb",borderWidth:1,padding:25,marginTop:20}}>
      <Image resizeMode="stretch" style={{width:'100%',height:300}} source={{uri:this.props.navigation.getParam('profilePicUrl')}} />
     
      </View>
      <View style={{margin:10,marginBottom:50}}>
      <Text style={{fontSize:25,fontWeight:"bold"}}>Dr {this.state.docName}</Text>
       <Text style={{fontSize:20,fontWeight:"bold",color:"#666666"}}>{this.state.docExperin}</Text>
       <Image resizeMode="contain" style={{height:35,width:75}} source={require('../assets/stars.png')} />
       <Text style={{fontSize:15,fontWeight:"bold",color:"#666666"}}>{this.state.exp} Years Experience</Text>

       <View style={{marginTop:20,borderColor:"#bbdefb",borderWidth:1,padding:10,backgroundColor:"#e3f2fd"}}>
       <View style={{flexDirection:"row",alignContent:"center",alignSelf:"center",alignItems:"center"}}>

        <Text style={{color:"#42a5f5",fontSize:20,flex:1,marginLeft:30}}>Call</Text>
        <Text style={{color:"#000000",fontSize:20,marginRight:30,fontWeight:"bold"}}>Rs. {this.state.call}</Text>
       </View>
       </View>
       <View style={{marginTop:20,borderColor:"#bbdefb",borderWidth:1,padding:10,backgroundColor:"#e3f2fd"}}>
       <View style={{flexDirection:"row",alignContent:"center",alignSelf:"center",alignItems:"center"}}>

        <Text style={{color:"#42a5f5",fontSize:20,flex:1,marginLeft:30}}>Video</Text>
        <Text style={{color:"#000000",fontSize:20,marginRight:30,fontWeight:"bold"}}>Rs. {this.state.video}</Text>
       </View>
       </View>
      
       
</View>

<TouchableOpacity onPress={this.consult.bind(this)}>
 <View style={{borderColor:"#26a69a",borderWidth:1,padding:10,backgroundColor:"#26a69a",borderRadius:5,marginLeft:10,marginRight:10}}>
       <Text style={{fontSize:20,fontWeight:"bold",color:"#ffffff",textAlign:"center"}}>Pay and Connect</Text>
       </View></TouchableOpacity>
       <View style={{borderColor:"#666666",borderWidth:1,padding:10,borderRadius:5,marginLeft:10,marginRight:10,width:'50%',alignContent:"center",alignItems:"center",alignSelf:"center",marginTop:10}}>
       <Text style={{fontWeight:"bold",color:"#666666",textAlign:"center"}}>View more details</Text>
       </View>
      
     </View>
     </ScrollView>

    );
  }
}
  
  const styles = StyleSheet.create({
    Maincontainer: {
      backgroundColor: '#ffffff',
      padding:20,
      marginTop:10,
      flex: 1,
      
    },
    HeadingText : {
      color : "#000000",
      fontSize : 18,
      fontWeight : "bold",
      marginTop : 10,

    },
    textInputContainer: {
      marginBottom: 10,
      width:5,
    },
    container: {
     
      height:80,
      width:80,
      padding:5,
      alignContent:"center",
      alignItems:"center",
      alignSelf:"center",
      borderBottomLeftRadius: 10,
      borderBottomRightRadius: 10,
      borderTopLeftRadius: 10,
      borderTopRightRadius: 10,
      // alignItems: 'center',
      justifyContent: 'center',
    
    },
    inoutBoxPhone: {
      height:40,
      width:250,
      paddingTop:10,
      paddingBottom:10,
      paddingHorizontal:10,
      backgroundColor:'#fff',
      borderBottomWidth : 0.5,
      

    },
    forgot : {
      alignItems : 'flex-end',
      justifyContent : 'flex-end',
    },
    inputEmail: {
      height:40,
      paddingTop:10,
      paddingBottom:10,
      paddingHorizontal:10,
      backgroundColor:'#fff',
      marginTop:50
    },
    inputPassword: {
      height:40,
      paddingTop:10,
      paddingBottom:10,
      paddingHorizontal:10,
      backgroundColor:'#fff',
      marginBottom:10,
      marginTop:10
    },
    welcomeText:{
       alignItems: 'center',
      justifyContent: 'center',
      fontSize : 25,
      fontWeight : "bold",
      alignSelf: 'stretch',
      textAlign: 'center',
      color : '#00B7A5',
      paddingHorizontal:10
    },
    horizontalContainer1:{

      alignContent:'center',
      alignItems: 'center',
      justifyContent: 'center',
     flexDirection : "row"
    },

    horizontalContainer:{

  
      marginTop : 10,
      alignContent:"flex-start",
      alignItems:"flex-start",
      alignSelf:"flex-start",
     flexDirection : "row"
    },
    welcomeText1:{
   
     fontSize : 25,
     fontWeight : "bold",
 
     flex:1,
     color : '#000000',
     paddingTop : 50,
     paddingHorizontal:10
   },
   welcomeText2:{
   
    fontSize : 35,
    fontWeight : "bold",
  
    color : '#4FA647',
    paddingTop : 50,
    paddingHorizontal:10
  },


    subText:{
      marginBottom:40,
      alignSelf: 'stretch',
      fontSize:16,
      textAlign: 'center',
      paddingHorizontal:10,
      color : '#2E2E2E'
    
    },
    subText1:{
      alignContent : "flex-start",
      marginBottom:5,
      marginTop: 10,
      fontSize:16,
      color : '#101010',
      paddingHorizontal:10
    },
    ButtonContainer:{
      alignItems:'center',
      alignContent:"center",
      alignSelf:"center",
      marginTop : 40,
      padding:25,
      width:100,
      flexDirection: 'row',
      justifyContent: 'center',
      backgroundColor:'#4FA647',  
      paddingTop:15,
      paddingBottom:15,
      paddingHorizontal:10,
      marginBottom:10,
      borderRadius:25
    },
    setFontSize: {
     
      fontSize : 18,
     
    },
    inputBoxEmail:{
      alignSelf: 'stretch',
      height:40,
      paddingTop:5,
      paddingBottom:5,
      paddingHorizontal:10,
      marginTop:15,
      fontSize:16,
      borderWidth: 1,
      borderColor : '#eee',
      backgroundColor:'#fff',
      borderRadius:1
    },
    inputBoxPassword:{
      alignSelf: 'stretch',
      height:40,
      paddingTop:5,
      paddingBottom:5,
      paddingHorizontal:10,
      marginTop:10,
      fontSize:16,
      borderWidth: 1,
      borderColor : '#eee',
      backgroundColor:'#fff',
      borderRadius:1
    },
    imgmenu:{
  
      height : 250,
      width : width_proportion,
      marginTop:25,
    },
    header:{
      alignSelf: 'stretch',
      height : 100,
      backgroundColor : '#00B7A5',
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.5,
      shadowRadius: 2,
      elevation: 2,
    },
    setColorRed : {
      color: '#fff'
    }
  });
  