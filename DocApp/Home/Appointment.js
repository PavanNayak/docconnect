import React from 'react';
import { AsyncStorage,Alert,Image,StyleSheet, Text, View, TextInput,TouchableOpacity,FlatList,ScrollView, KeyboardAvoidingView} from 'react-native';
import { ToastAndroid } from "react-native";
import axios from 'axios';
import Url from '../Utils/config';
import moment from 'moment'; 
import CalendarStrip from 'react-native-calendar-strip';
import { StackActions, NavigationActions } from 'react-navigation';
const width_proportion = '100%';
var AuthStr='';
var time=[
  {
    "name":"08:00 AM"
  },
  {
    "name":"08:30 AM"
  },
  {
    "name":"09:00 AM"
  },
  {
    "name":"09:30 AM"
  },
  {
    "name":"10:00 AM"
  },
  {
    "name":"10:30 AM"
  },
  {
    "name":"11:00 AM"
  },
  {
    "name":"11:30 AM"
  },
  {
    "name":"12:00 PM"
  },
  {
    "name":"12:30 PM"
  },
  {
    "name":"01:00 PM"
  },
  {
    "name":"01:30 PM"
  },
  {
    "name":"02:00 PM"
  },
  {
    "name":"02:30 PM"
  },
  {
    "name":"03:00 PM"
  },
  
]
export default class Appointment extends React.Component{
  static navigationOptions = {
    header: null ,

  };


 

  constructor(props) {
    super(props);
    this.state = {
      email : '',
      password : '',
      catList: '',
      user_id:'',
      doctor_id : '',
      mode:'',
    }

    let startDate = moment(); // today
    this.setState({minDate :startDate });
    // Create a week's worth of custom date styles and marked dates.
    let customDatesStyles = [];
    let markedDates = [];
    for (let i=0; i<7; i++) {
      let date = startDate.clone().add(i, 'days');

      customDatesStyles.push({
        startDate: date, // Single date since no endDate provided
        dateNameStyle: {color: 'blue'},
        dateNumberStyle: {color: 'purple'},
        // Random color...
        dateContainerStyle: { backgroundColor: `#${(`#00000${(Math.random() * (1 << 24) | 0).toString(16)}`).slice(-6)}` },
      });

      let dots = [];
      let lines = [];

      if (i % 2) {
        lines.push({
          color: 'cyan',
          selectedColor: 'orange',
        });
      }
      else {
        dots.push({
          color: 'red',
          selectedColor: 'yellow',
        });
      }
      markedDates.push({
        date,
        dots,
        lines
      });
    }

    this.state = {
      selectedDate: '',
      customDatesStyles,
      markedDates,
      startDate,
    };
  }

  componentDidMount(){
    this.getId();
  }
  async getId(){
    AsyncStorage.getItem('user_id').then((id)=>
    this.setState({user_id:id}));
    AsyncStorage.getItem('mode').then((mode)=>
    this.setState({mode:mode}));

    AsyncStorage.getItem('doctor_id').then((id)=>
    this.setState({doctor_id:id}));
  }

  async appoint(time){
    self=this;
    console.log({  "date" : this.state.formattedDate,
    "time" : time,
    "modeOfConsultation" : this.state.mode,
    "bookingDoctorId" : this.state.doctor_id})
    await AsyncStorage.getItem('token').then((token)=>
    {
     AuthStr = 'Bearer '+token; 
     console.log(AuthStr)
 
    axios.post(Url.BaseUrl + "appointment/new/"+self.state.user_id,{
      "date" : self.state.formattedDate,
      "time" : time,
      "modeOfConsultation" : self.state.mode,
      "bookingDoctorId" : self.state.doctor_id
  },{ headers: { auth: AuthStr } })
   
  .then(function (responseJson) {
        console.log("Appointment Response"+JSON.stringify(responseJson.data));
        self.createOrder(responseJson.data.appointmentdata._id);
     
       
      
      })
      .catch((error) => {
        console.error(error);
      }); 
   // this.p)rops.navigation.navigate("Dashbord");
  })

}
  
async createOrder(appointmentId){
  let self=this;
  await AsyncStorage.getItem('token').then((token)=>
    {
     AuthStr = 'Bearer '+token; 
     console.log(AuthStr)
 
    axios.post(Url.BaseUrl + "payment/createOrder",{
      "amount" : "12000",
      "receipt" : "appointment fee",
      "doctorId" :self.state.doctor_id,
      "appointmentId" : appointmentId,
      "patientId" : self.state.user_id
  },{ headers: { auth: AuthStr } })
   
  .then(function (responseJson) {
        console.log("CreateOrder Response"+JSON.stringify(responseJson.data));
        ToastAndroid.showWithGravityAndOffset(
          responseJson.data.message,
     ToastAndroid.LONG,
     ToastAndroid.BOTTOM,
     25,
     50
     );
       
      
      })
      .catch((error) => {
        console.error(error);
      }); 
   // this.p)rops.navigation.navigate("Dashbord");
  })

}


  onDateSelected = date => {
    this.setState({ formattedDate: date.format('DD-MM-YYYY')});
    let selectedDate =date.format('YYYY-MM-DD');
    let startDate =moment(new Date()).format('YYYY-MM-DD');
    var ms = moment(selectedDate,"YYYY-MM-DD").diff(moment(startDate,"YYYY-MM-DD"));
    // var mss=moment.range(moment(new Date()).format('YYYY-MM-DD'),moment(selectedDate,"YYYY-MM-DD"));
    console.log(ms);
  
   
  }
  
  // doctorDetail(id,image){
  //   this.props.navigation.navigate("DoctorDetail",{'id':id,'profilePicUrl':image});
  // }

  goback(){
    this.props.navigation.navigate("Home");
  }

  
    render(){
      const {navigate} = this.props.navigation;
    return (
   
     <View style={styles.Maincontainer}>
      <View style={{flexDirection:"row",alignContent:"center",alignSelf:"center",alignItems:"center",paddingTop:10,backgroundColor:"#3f51b5"}}>
      <TouchableOpacity onPress={this.goback.bind(this)}><Image resizeMode="contain" style={{height:35,width:35,tintColor:"#ffffff"}} source={require('../assets/left-arrow.png')} /></TouchableOpacity>
      <Text style={{fontSize:20,fontWeight:"bold",flex:1,marginLeft:10,color:"#ffffff"}}>Appointment</Text>
      </View>
     
      <CalendarStrip
          scrollable
          calendarAnimation={{type: 'sequence', duration: 30}}
          daySelectionAnimation={{type: 'background', duration: 300, highlightColor: '#ffffff'}}
          style={{height:200, paddingTop: 20, paddingBottom: 10}}
          calendarHeaderStyle={{color: 'white'}}
          calendarColor={'#3f51b5'}
          dateNumberStyle={{color: 'white'}}
          dateNameStyle={{color: 'white'}}
          iconContainer={{flex: 0.1}}
          // customDatesStyles={this.state.customDatesStyles}
          // markedDates={this.state.markedDates}
          // datesBlacklist={this.datesBlacklistFunc}
          // minDate={this.state.minDate}
          onDateSelected={this.onDateSelected}
          useIsoWeekday={false}
        />
        <View style={{alignContent:"center",alignItems:"center",alignSelf:"center",marginTop:20}}>
 <FlatList
        data={time}
       
        numColumns ={3}
        ItemSeparatorComponent = {this.FlatListItemSeparator}
        renderItem={({item,index}) => 
      
        <TouchableOpacity onPress={()=>this.appoint(item.name)}>
        <View style={{margin:5,marginTop:10,flexDirection:"row",borderWidth:1,borderColor:"#e0e0e0",borderRadius:22,padding:5,backgroundColor:"#eeeeee"}}>
          {/* <Text ref='myText'>{item[i].SubCategoryID}</Text> */}
        
       
       <Text style={{ alignContent:"center",
       alignItems:"center",
       alignSelf:"center",marginLeft:10,marginRight:10,textAlign:"center"}} >{item.name}</Text>
       </View>
       </TouchableOpacity>
      
  }
       keyExtractor={(item, index) => index}
      />
     
      
     </View>
     </View>
     

    );
  }
}
  
  const styles = StyleSheet.create({
    Maincontainer: {
      backgroundColor: '#ffffff',
     
      marginTop:30,
      flex: 1,
      
    },
    HeadingText : {
      color : "#000000",
      fontSize : 18,
      fontWeight : "bold",
      marginTop : 30,

    },
    textInputContainer: {
      marginBottom: 10,
      width:5,
    },
    container: {
     
      height:80,
      width:80,
      padding:5,
      alignContent:"center",
      alignItems:"center",
      alignSelf:"center",
      borderBottomLeftRadius: 10,
      borderBottomRightRadius: 10,
      borderTopLeftRadius: 10,
      borderTopRightRadius: 10,
      // alignItems: 'center',
      justifyContent: 'center',
    
    },
    inoutBoxPhone: {
      height:40,
      width:250,
      paddingTop:10,
      paddingBottom:10,
      paddingHorizontal:10,
      backgroundColor:'#fff',
      borderBottomWidth : 0.5,
      

    },
    forgot : {
      alignItems : 'flex-end',
      justifyContent : 'flex-end',
    },
    inputEmail: {
      height:40,
      paddingTop:10,
      paddingBottom:10,
      paddingHorizontal:10,
      backgroundColor:'#fff',
      marginTop:50
    },
    inputPassword: {
      height:40,
      paddingTop:10,
      paddingBottom:10,
      paddingHorizontal:10,
      backgroundColor:'#fff',
      marginBottom:10,
      marginTop:10
    },
    welcomeText:{
       alignItems: 'center',
      justifyContent: 'center',
      fontSize : 25,
      fontWeight : "bold",
      alignSelf: 'stretch',
      textAlign: 'center',
      color : '#00B7A5',
      paddingHorizontal:10
    },
    horizontalContainer1:{

      alignContent:'center',
      alignItems: 'center',
      justifyContent: 'center',
     flexDirection : "row"
    },

    horizontalContainer:{

  
      marginTop : 10,
      alignContent:"flex-start",
      alignItems:"flex-start",
      alignSelf:"flex-start",
     flexDirection : "row"
    },
    welcomeText1:{
   
     fontSize : 25,
     fontWeight : "bold",
 
     flex:1,
     color : '#000000',
     paddingTop : 50,
     paddingHorizontal:10
   },
   welcomeText2:{
   
    fontSize : 35,
    fontWeight : "bold",
  
    color : '#4FA647',
    paddingTop : 50,
    paddingHorizontal:10
  },


    subText:{
      marginBottom:40,
      alignSelf: 'stretch',
      fontSize:16,
      textAlign: 'center',
      paddingHorizontal:10,
      color : '#2E2E2E'
    
    },
    subText1:{
      alignContent : "flex-start",
      marginBottom:5,
      marginTop: 10,
      fontSize:16,
      color : '#101010',
      paddingHorizontal:10
    },
    ButtonContainer:{
      alignItems:'center',
      alignContent:"center",
      alignSelf:"center",
      marginTop : 40,
      padding:25,
      width:100,
      flexDirection: 'row',
      justifyContent: 'center',
      backgroundColor:'#4FA647',  
      paddingTop:15,
      paddingBottom:15,
      paddingHorizontal:10,
      marginBottom:10,
      borderRadius:25
    },
    setFontSize: {
     
      fontSize : 18,
     
    },
    inputBoxEmail:{
      alignSelf: 'stretch',
      height:40,
      paddingTop:5,
      paddingBottom:5,
      paddingHorizontal:10,
      marginTop:15,
      fontSize:16,
      borderWidth: 1,
      borderColor : '#eee',
      backgroundColor:'#fff',
      borderRadius:1
    },
    inputBoxPassword:{
      alignSelf: 'stretch',
      height:40,
      paddingTop:5,
      paddingBottom:5,
      paddingHorizontal:10,
      marginTop:10,
      fontSize:16,
      borderWidth: 1,
      borderColor : '#eee',
      backgroundColor:'#fff',
      borderRadius:1
    },
    imgmenu:{
  
      height : 250,
      width : width_proportion,
      marginTop:25,
    },
    header:{
      alignSelf: 'stretch',
      height : 100,
      backgroundColor : '#00B7A5',
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.5,
      shadowRadius: 2,
      elevation: 2,
    },
    setColorRed : {
      color: '#fff'
    }
  });
  