import React from 'react';
import { AsyncStorage,Alert,Image,StyleSheet, Text, View, TextInput,TouchableOpacity,FlatList,ScrollView, KeyboardAvoidingView} from 'react-native';
import { ToastAndroid } from "react-native";
import axios from 'axios';
import Url from '../Utils/config';
import { StackActions, NavigationActions } from 'react-navigation';
const width_proportion = '100%';
var AuthStr='';
var doctors=[
  {
    "name":"Dr. Usha Kiran",
    "speciality":"Gynacologist",
    "image":require('../assets/download.jpg'),
    "date":"12/08/2020"

  },
  {
    "name":"Dr. Shakthi Prasad",
    "speciality":"Neurologist",
    "image":require('../assets/d2.jpg'),
    "date":"12/08/2020"

  },
   {
    "name":"Dr. Soumya",
    "speciality":"General",
    "image":require('../assets/download.jpg'),
    "date":"12/08/2020"

  },
  {
    "name":"Dr. Usha Kiran",
    "speciality":"Gynacologist",
    "image":require('../assets/download.jpg'),
    "date":"12/08/2020"

  },
  {
    "name":"Dr. Shakthi Prasad",
    "speciality":"Neurologist",
    "image":require('../assets/d2.jpg'),
    "date":"12/08/2020"

  },
   {
    "name":"Dr. Soumya",
    "speciality":"General",
    "image":require('../assets/download.jpg'),
    "date":"12/08/2020"

  }


]
export default class DoctorList extends React.Component{
  static navigationOptions = {
    header: null ,

  };


 

  constructor(props) {
    super(props);
    this.state = {
      email : '',
      password : '',
      docList: '',
    }
  }

  
  doctorDetail(id,image){
    this.props.navigation.navigate("DoctorDetail",{'id':id,'profilePicUrl':image});
  }


  async getDoctorList(){
    self=this;
    await AsyncStorage.getItem('token').then((token)=>
    {
     AuthStr = 'Bearer '+token; 
     console.log(AuthStr)
    axios.get(Url.BaseUrl + "doctor/expertIn/list?expertIn="+this.props.navigation.getParam('category'),{ headers: { auth: AuthStr } })
   
  .then(function (responseJson) {
        console.log("CategoryResp"+JSON.stringify(responseJson.data));
       self.setState({docList:responseJson.data.doctorsList});
      
        
      })
      .catch((error) => {
        console.error(error);
      });
    }
    )
     
  }
  componentDidMount(){
    this.getDoctorList();
  }

  goback(){
    this.props.navigation.navigate("Home");
  }

  
    render(){
      const {navigate} = this.props.navigation;
    return (
   
     <View style={styles.Maincontainer}>
      <View style={{flexDirection:"row",alignContent:"center",alignSelf:"center",alignItems:"center",marginTop:10}}>
      <TouchableOpacity onPress={this.goback.bind(this)}><Image resizeMode="contain" style={{height:35,width:35}} source={require('../assets/left-arrow.png')} /></TouchableOpacity>
      <Text style={{fontSize:20,fontWeight:"bold",flex:1,marginLeft:10}}>{this.props.navigation.getParam('category')}</Text>
      </View>
      {
        this.state.docList.length==0 ? 
        <View>
<Text style={[styles.HeadingText,{marginBottom:5}]}>Sorry! No doctors available!</Text>
<Text>Doctor with this specialization is not available at the moment</Text>
</View>
        :
        <FlatList
        data={this.state.docList}
        
        ItemSeparatorComponent = {this.FlatListItemSeparator}
        renderItem={({item,index}) => 
        <View>
        { item.documentApprovalStatus=='Approved' && item.serviceStatus=='Active' ?
        <TouchableOpacity onPress={()=>this.doctorDetail(item._id,item.profilePicUrl)}>
        <View style={{margin:5,marginTop:10,flexDirection:"row",backgroundColor:"#e3f2fd",borderColor:"#bbdefb",borderWidth:1,padding:10}}>
          {/* <Text ref='myText'>{item[i].SubCategoryID}</Text> */}
        
          <Image resizeMode="contain" style={{height:70,width:70,alignContent:"center",alignItems:"center",alignSelf:"center"}} source={{uri:item.profilePicUrl}} />
       <View style={{flex:1,marginLeft:50}}>
       <Text style={{
     marginTop:5,fontSize:14,fontWeight:"bold"}} >Dr {item.name}</Text>
        <Text style={{marginTop:5,
      fontSize:12,color:"#666666"}} >{item.expertIn}</Text>
       <Image resizeMode="contain" style={{height:30,width:70}} source={require('../assets/stars.png')} />
     
       </View>
       </View>
       </TouchableOpacity>
        
        :

          null

        }
        </View>
      

    
  }
       keyExtractor={(item, index) => index}
      />
      }
      
     
     </View>
     

    );
  }
}
  
  const styles = StyleSheet.create({
    Maincontainer: {
      backgroundColor: '#ffffff',
      padding:20,
      marginTop:10,
      flex: 1,
      
    },
    HeadingText : {
      color : "#000000",
      fontSize : 18,
      fontWeight : "bold",
      marginTop : 30,

    },
    textInputContainer: {
      marginBottom: 10,
      width:5,
    },
    container: {
     
      height:80,
      width:80,
      padding:5,
      alignContent:"center",
      alignItems:"center",
      alignSelf:"center",
      borderBottomLeftRadius: 10,
      borderBottomRightRadius: 10,
      borderTopLeftRadius: 10,
      borderTopRightRadius: 10,
      // alignItems: 'center',
      justifyContent: 'center',
    
    },
    inoutBoxPhone: {
      height:40,
      width:250,
      paddingTop:10,
      paddingBottom:10,
      paddingHorizontal:10,
      backgroundColor:'#fff',
      borderBottomWidth : 0.5,
      

    },
    forgot : {
      alignItems : 'flex-end',
      justifyContent : 'flex-end',
    },
    inputEmail: {
      height:40,
      paddingTop:10,
      paddingBottom:10,
      paddingHorizontal:10,
      backgroundColor:'#fff',
      marginTop:50
    },
    inputPassword: {
      height:40,
      paddingTop:10,
      paddingBottom:10,
      paddingHorizontal:10,
      backgroundColor:'#fff',
      marginBottom:10,
      marginTop:10
    },
    welcomeText:{
       alignItems: 'center',
      justifyContent: 'center',
      fontSize : 25,
      fontWeight : "bold",
      alignSelf: 'stretch',
      textAlign: 'center',
      color : '#00B7A5',
      paddingHorizontal:10
    },
    horizontalContainer1:{

      alignContent:'center',
      alignItems: 'center',
      justifyContent: 'center',
     flexDirection : "row"
    },

    horizontalContainer:{

  
      marginTop : 10,
      alignContent:"flex-start",
      alignItems:"flex-start",
      alignSelf:"flex-start",
     flexDirection : "row"
    },
    welcomeText1:{
   
     fontSize : 25,
     fontWeight : "bold",
 
     flex:1,
     color : '#000000',
     paddingTop : 50,
     paddingHorizontal:10
   },
   welcomeText2:{
   
    fontSize : 35,
    fontWeight : "bold",
  
    color : '#4FA647',
    paddingTop : 50,
    paddingHorizontal:10
  },


    subText:{
      marginBottom:40,
      alignSelf: 'stretch',
      fontSize:16,
      textAlign: 'center',
      paddingHorizontal:10,
      color : '#2E2E2E'
    
    },
    subText1:{
      alignContent : "flex-start",
      marginBottom:5,
      marginTop: 10,
      fontSize:16,
      color : '#101010',
      paddingHorizontal:10
    },
    ButtonContainer:{
      alignItems:'center',
      alignContent:"center",
      alignSelf:"center",
      marginTop : 40,
      padding:25,
      width:100,
      flexDirection: 'row',
      justifyContent: 'center',
      backgroundColor:'#4FA647',  
      paddingTop:15,
      paddingBottom:15,
      paddingHorizontal:10,
      marginBottom:10,
      borderRadius:25
    },
    setFontSize: {
     
      fontSize : 18,
     
    },
    inputBoxEmail:{
      alignSelf: 'stretch',
      height:40,
      paddingTop:5,
      paddingBottom:5,
      paddingHorizontal:10,
      marginTop:15,
      fontSize:16,
      borderWidth: 1,
      borderColor : '#eee',
      backgroundColor:'#fff',
      borderRadius:1
    },
    inputBoxPassword:{
      alignSelf: 'stretch',
      height:40,
      paddingTop:5,
      paddingBottom:5,
      paddingHorizontal:10,
      marginTop:10,
      fontSize:16,
      borderWidth: 1,
      borderColor : '#eee',
      backgroundColor:'#fff',
      borderRadius:1
    },
    imgmenu:{
  
      height : 250,
      width : width_proportion,
      marginTop:25,
    },
    header:{
      alignSelf: 'stretch',
      height : 100,
      backgroundColor : '#00B7A5',
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.5,
      shadowRadius: 2,
      elevation: 2,
    },
    setColorRed : {
      color: '#fff'
    }
  });
  