import React from 'react';
import { Alert,Image,StyleSheet, Text, View, TextInput,TouchableOpacity,FlatList,ScrollView, KeyboardAvoidingView, AsyncStorage} from 'react-native';
import { ToastAndroid } from "react-native";
import axios from 'axios';
import Url from '../Utils/config';
import { StackActions, NavigationActions } from 'react-navigation';
const width_proportion = '100%';
var AuthStr='';

// var speciality=[
//   {
//     "image":require('../assets/dental.png'),
//     "name":"Dental"
//   },
//   {
//     "image":require('../assets/heart.png'),
//     "name":"Cardiologist"
//   },
//   {
//     "image":require('../assets/dental.png'),
//     "name":"Neurologist"
//   },
//   {
//     "image":require('../assets/heart.png'),
//     "name":"General"
//   },
//   {
//     "image":require('../assets/dental.png'),
//     "name":"Dental"
//   },
//   {
//     "image":require('../assets/heart.png'),
//     "name":"Cardiologist"
//   },
//   {
//     "image":require('../assets/dental.png'),
//     "name":"Neurologist"
//   },
//   {
//     "image":require('../assets/heart.png'),
//     "name":"General"
//   },
// ]
export default class Home extends React.Component{
  static navigationOptions = {
    header: null ,

  };


 

  constructor(props) {
    super(props);
    this.state = {
      email : '',
      password : '',
      newdoctors:[],
      searchText:'',
    }
    
  }

  async getCategoryList(){
    self=this;
    await AsyncStorage.getItem('token').then((token)=>
    {
     AuthStr = 'Bearer '+token; 
     console.log(AuthStr)
    axios.get(Url.BaseUrl + "category/list",{ headers: { auth: AuthStr } })
   
  .then(function (responseJson) {
        console.log("CategoryResp"+JSON.stringify(responseJson.data));
       self.setState({speciality:responseJson.data.categories});
      
        
      })
      .catch((error) => {
        console.error(error);
      });
    }
    )
     
  }

  async getDoctorsList(){
    self=this;
    await AsyncStorage.getItem('token').then((token)=>
    {
     AuthStr = 'Bearer '+token; 
     console.log(AuthStr)
    axios.get(Url.BaseUrl + "doctor/list",{ headers: { auth: AuthStr } })
   
  .then(function (responseJson) {
        console.log("DocResp"+JSON.stringify(responseJson.data));
       self.setState({doctors:responseJson.data.doctorsList});
      
        
      })
      .catch((error) => {
        console.error(error);
      });
    }
    )
     
  }

  searchDoctor(text){
    this.setState({searchText:text})
    console.log(this.state.doctors.length)
    for(var i=0;i<this.state.doctors.length;i++){
      if(text==this.state.doctors[i].name){
        console.log(this.state.doctors[i])
        var newStateArray = this.state.newdoctors.slice();
        newStateArray.push(this.state.doctors[i]);
        this.setState({newdoctors : newStateArray});
        // this.setState({newdoctors:this.state.doctors[i]});
      }
      else
      {
        // this.setState({newdoctors:[]})
      }
    }
  }

  onPressMore(){
    this.props.navigation.navigate("PopularCategories");
  }

  onPressCategory(category){
    console.log(category)
    this.props.navigation.navigate("DoctorList",{'category':category});
  }
  doctorDetail(id,image){
    this.props.navigation.navigate("DoctorDetail",{'id':id,'profilePicUrl':image});
  }

  componentDidMount(){
    this.getCategoryList();
    this.getDoctorsList();
  }
  
    render(){
      const {navigate} = this.props.navigation;
    return (
    <ScrollView>
     <View style={styles.Maincontainer}>
       <Text style={{fontSize:25,fontWeight:"bold",marginTop:50}}>Find your Desired</Text>
       <Text style={{fontSize:30,fontWeight:"bold"}}>Doctor</Text>

       <TextInput 
          placeholder ="Search Specialists"
          placeholderTextColor="#666666"
          style={{ 
            padding:10,
          
            marginTop:10,
            borderColor:'#eeeeee',
            borderRadius:10,
            color:"#eeeeee",
            backgroundColor:"#eeeeee",
            }}
            onChangeText={(text) => {
                 this.searchDoctor(text);       
            }
          }
          />
        <View style={{flexDirection:"row",marginTop:20,alignContent:"center",alignItems:"center",alignSelf:"center"}}>
        <Text style={{fontSize:16,fontWeight:"bold",flex:1}}>Popular Categories</Text>
        <TouchableOpacity onPress={()=>this.onPressMore()}><Text style={{fontWeight:"bold",color:"#ffffff",backgroundColor:"#000000",borderRadius:5,padding:5}}>more</Text></TouchableOpacity>
        </View>
        <View style={{height:100}}>
        <FlatList
        horizontal data={this.state.speciality}
        
        ItemSeparatorComponent = {this.FlatListItemSeparator}
        renderItem={({item,index}) => 
        index%2==0?
       
        <TouchableOpacity onPress={()=>this.onPressCategory(item.name)}>
        <View style={{height:70,margin:5,width:80,marginTop:10}}>
          {/* <Text ref='myText'>{item[i].SubCategoryID}</Text> */}
        
          <Image resizeMode="contain" style={{height:40,width:40,alignContent:"center",alignItems:"center",alignSelf:"center"}} source={require('../assets/dental.png')} />
       <Text style={{ alignContent:"center",
       alignItems:"center",marginTop:5,
       alignSelf:"center",width:100,textAlign:'center'}} >{item.name}</Text>
       </View>
       </TouchableOpacity>
       :
       <TouchableOpacity onPress={()=>this.onPressCategory(item.name)}>
       <View style={{height:70,margin:5,width:80,marginTop:10}}>
          {/* <Text ref='myText'>{item[i].SubCategoryID}</Text> */}
        
          <Image resizeMode="contain" style={{height:40,width:40,alignContent:"center",alignItems:"center",alignSelf:"center"}} source={require('../assets/heart.png')} />
       <Text style={{ alignContent:"center",
       alignItems:"center",marginTop:5,
       alignSelf:"center",width:100,textAlign:'center'}} >{item.name}</Text>
       </View>
      </TouchableOpacity>

    
  }
       keyExtractor={(item, index) => index}
      />
 </View>
 <Text style={{fontSize:16,fontWeight:"bold",flex:1}}>Top Rated Doctors</Text>

 {
   this.state.searchText.length==0 ? 
   <FlatList
        data={this.state.doctors}
        
        ItemSeparatorComponent = {this.FlatListItemSeparator}
        renderItem={({item,index}) => 
        <View>
        {/* { item.documentApprovalStatus=='Approved' && item.serviceStatus=='Active' ? */}
        <TouchableOpacity onPress={()=>this.doctorDetail(item._id,item.profilePicUrl)}>
        <View style={{margin:5,marginTop:10,flexDirection:"row",backgroundColor:"#e3f2fd",borderColor:"#bbdefb",borderWidth:1,padding:10}}>
          {/* <Text ref='myText'>{item[i].SubCategoryID}</Text> */}
        
          <Image resizeMode="contain" style={{height:70,width:70,alignContent:"center",alignItems:"center",alignSelf:"center"}} source={{uri:item.profilePicUrl}} />
       <View style={{flex:1,marginLeft:50}}>
       <Text style={{
     marginTop:5,fontSize:14,fontWeight:"bold"}} >Dr {item.name}</Text>
        <Text style={{marginTop:5,
      fontSize:12,color:"#666666"}} >{item.expertIn}</Text>
       <Image resizeMode="contain" style={{height:30,width:70}} source={require('../assets/stars.png')} />
     
       </View>
       </View>
       </TouchableOpacity>
        
     
        </View>
      

    
  }
       keyExtractor={(item, index) => index}
      />
   :
   <FlatList
        data={this.state.newdoctors}
        
        ItemSeparatorComponent = {this.FlatListItemSeparator}
        renderItem={({item,index}) => 
        <View>
        { item.documentApprovalStatus=='Approved' && item.serviceStatus=='Active' ?
        <TouchableOpacity onPress={()=>this.doctorDetail(item._id,item.profilePicUrl)}>
        <View style={{margin:5,marginTop:10,flexDirection:"row",backgroundColor:"#e3f2fd",borderColor:"#bbdefb",borderWidth:1,padding:10}}>
          {/* <Text ref='myText'>{item[i].SubCategoryID}</Text> */}
        
          <Image resizeMode="contain" style={{height:70,width:70,alignContent:"center",alignItems:"center",alignSelf:"center"}} source={{uri:item.profilePicUrl}} />
       <View style={{flex:1,marginLeft:50}}>
       <Text style={{
     marginTop:5,fontSize:14,fontWeight:"bold"}} >Dr {item.name}</Text>
        <Text style={{marginTop:5,
      fontSize:12,color:"#666666"}} >{item.expertIn}</Text>
       <Image resizeMode="contain" style={{height:30,width:70}} source={require('../assets/stars.png')} />
     
       </View>
       </View>
       </TouchableOpacity>
        
        :

          null

        }
        </View>
      

    
  }
       keyExtractor={(item, index) => index}
      />

 }
 
     </View>
     </ScrollView>

    );
  }
}
  
  const styles = StyleSheet.create({
    Maincontainer: {
      backgroundColor: '#ffffff',
      padding:20,
      marginTop:10,
      flex: 1,
      
    },
    HeadingText : {
      color : "#000000",
      fontSize : 18,
      fontWeight : "bold",
      marginTop : 10,

    },
    textInputContainer: {
      marginBottom: 10,
      width:5,
    },
    container: {
     
      height:80,
      width:80,
      padding:5,
      alignContent:"center",
      alignItems:"center",
      alignSelf:"center",
      borderBottomLeftRadius: 10,
      borderBottomRightRadius: 10,
      borderTopLeftRadius: 10,
      borderTopRightRadius: 10,
      // alignItems: 'center',
      justifyContent: 'center',
    
    },
    inoutBoxPhone: {
      height:40,
      width:250,
      paddingTop:10,
      paddingBottom:10,
      paddingHorizontal:10,
      backgroundColor:'#fff',
      borderBottomWidth : 0.5,
      

    },
    forgot : {
      alignItems : 'flex-end',
      justifyContent : 'flex-end',
    },
    inputEmail: {
      height:40,
      paddingTop:10,
      paddingBottom:10,
      paddingHorizontal:10,
      backgroundColor:'#fff',
      marginTop:50
    },
    inputPassword: {
      height:40,
      paddingTop:10,
      paddingBottom:10,
      paddingHorizontal:10,
      backgroundColor:'#fff',
      marginBottom:10,
      marginTop:10
    },
    welcomeText:{
       alignItems: 'center',
      justifyContent: 'center',
      fontSize : 25,
      fontWeight : "bold",
      alignSelf: 'stretch',
      textAlign: 'center',
      color : '#00B7A5',
      paddingHorizontal:10
    },
    horizontalContainer1:{

      alignContent:'center',
      alignItems: 'center',
      justifyContent: 'center',
     flexDirection : "row"
    },

    horizontalContainer:{

  
      marginTop : 10,
      alignContent:"flex-start",
      alignItems:"flex-start",
      alignSelf:"flex-start",
     flexDirection : "row"
    },
    welcomeText1:{
   
     fontSize : 25,
     fontWeight : "bold",
 
     flex:1,
     color : '#000000',
     paddingTop : 50,
     paddingHorizontal:10
   },
   welcomeText2:{
   
    fontSize : 35,
    fontWeight : "bold",
  
    color : '#4FA647',
    paddingTop : 50,
    paddingHorizontal:10
  },


    subText:{
      marginBottom:40,
      alignSelf: 'stretch',
      fontSize:16,
      textAlign: 'center',
      paddingHorizontal:10,
      color : '#2E2E2E'
    
    },
    subText1:{
      alignContent : "flex-start",
      marginBottom:5,
      marginTop: 10,
      fontSize:16,
      color : '#101010',
      paddingHorizontal:10
    },
    ButtonContainer:{
      alignItems:'center',
      alignContent:"center",
      alignSelf:"center",
      marginTop : 40,
      padding:25,
      width:100,
      flexDirection: 'row',
      justifyContent: 'center',
      backgroundColor:'#4FA647',  
      paddingTop:15,
      paddingBottom:15,
      paddingHorizontal:10,
      marginBottom:10,
      borderRadius:25
    },
    setFontSize: {
     
      fontSize : 18,
     
    },
    inputBoxEmail:{
      alignSelf: 'stretch',
      height:40,
      paddingTop:5,
      paddingBottom:5,
      paddingHorizontal:10,
      marginTop:15,
      fontSize:16,
      borderWidth: 1,
      borderColor : '#eee',
      backgroundColor:'#fff',
      borderRadius:1
    },
    inputBoxPassword:{
      alignSelf: 'stretch',
      height:40,
      paddingTop:5,
      paddingBottom:5,
      paddingHorizontal:10,
      marginTop:10,
      fontSize:16,
      borderWidth: 1,
      borderColor : '#eee',
      backgroundColor:'#fff',
      borderRadius:1
    },
    imgmenu:{
  
      height : 250,
      width : width_proportion,
      marginTop:25,
    },
    header:{
      alignSelf: 'stretch',
      height : 100,
      backgroundColor : '#00B7A5',
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.5,
      shadowRadius: 2,
      elevation: 2,
    },
    setColorRed : {
      color: '#fff'
    }
  });
  