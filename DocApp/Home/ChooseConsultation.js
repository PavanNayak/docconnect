import React from 'react';
import { Alert,Image,StyleSheet, Text, View, TextInput,TouchableOpacity,FlatList,ScrollView, KeyboardAvoidingView,AsyncStorage} from 'react-native';
import { ToastAndroid } from "react-native";

import { StackActions, NavigationActions } from 'react-navigation';
const width_proportion = '100%';

export default class ChooseConsultation extends React.Component{
  static navigationOptions = {
    header: null ,

  };


 

  constructor(props) {
    super(props);
    this.state = {
      email : '',
      password : '',
  
    }
  }


    onClickVideo(mode){

      AsyncStorage.setItem('mode',mode).then(()=>
      this.props.navigation.navigate("Appointment")
    )
     
    }
  
    render(){
      const {navigate} = this.props.navigation;
    return (
      <ScrollView>
     <View style={styles.Maincontainer}>
       <View style={{flex:1}}></View>
      <Text style={{fontSize:30,fontWeight:"bold",marginTop:50}}>Choose the</Text>
      <Text style={{fontSize:30,fontWeight:"bold"}}>Consultation</Text>
      <Text style={{fontSize:30,fontWeight:"bold"}}>Format</Text>
      <Text style={{marginRight:50,color:"#666666",fontWeight:"bold",marginTop:10,marginBottom:10}}>Charges applicable as per the doctors consultation slab</Text>

      <Text style={{marginRight:50,color:"#000000",fontWeight:"bold",marginTop:30,marginBottom:10,fontSize:18}}>Select one to continue</Text>

      <View style={{flexDirection:"row",marginTop:30,alignContent:"center",alignItems:"center",alignSelf:"center"}}>
      <Image resizeMode="contain" style={{height:50,width:50}} source={require('../assets/email.png')} />
      <View style={{marginLeft:20,flex:1}}>
      <Text style={{marginRight:50,color:"#000000",fontWeight:"bold",fontSize:19}}>Chat</Text>
      <Text style={{marginRight:70,color:"#666666",fontWeight:"bold",marginTop:1}}>Where you can just bussy to talk</Text>

      </View>
      </View>
      <TouchableOpacity onPress={()=>this.onClickVideo("call")}>
      <View style={{flexDirection:"row",marginTop:30,alignContent:"center",alignItems:"center",alignSelf:"center"}}>
      <Image resizeMode="contain" style={{height:50,width:50}} source={require('../assets/telephone.png')} />
      <View style={{marginLeft:20,flex:1}}>
      <Text style={{marginRight:50,color:"#000000",fontWeight:"bold",fontSize:19}}>Audio</Text>
      <Text style={{marginRight:70,color:"#666666",fontWeight:"bold",marginTop:1}}>Same as phone call but you can share the photo</Text>

      </View>
      </View>
      </TouchableOpacity>

      <TouchableOpacity onPress={()=>this.onClickVideo("video")}>
      <View style={{flexDirection:"row",marginTop:30,alignContent:"center",alignItems:"center",alignSelf:"center",marginBottom:200}}>
      <Image resizeMode="contain" style={{height:50,width:50}} source={require('../assets/play.png')} />
      <View style={{marginLeft:20,flex:1}}>
      <Text style={{marginRight:50,color:"#000000",fontWeight:"bold",fontSize:19}}>Video</Text>
      <Text style={{marginRight:70,color:"#666666",fontWeight:"bold",marginTop:1}}>You and Doctor will see each other</Text>

      </View>
      </View>
      </TouchableOpacity>
      <View style={{flex:1}}></View>
         </View>
         </ScrollView>

    );
  }
}
  
  const styles = StyleSheet.create({
    Maincontainer: {
      backgroundColor: '#ffffff',
      padding:40,
      marginTop:10,
      flex: 1,
      
    },
    HeadingText : {
      color : "#000000",
      fontSize : 18,
      fontWeight : "bold",
      marginTop : 10,

    },
    textInputContainer: {
      marginBottom: 10,
      width:5,
    },
    container: {
     
      height:80,
      width:80,
      padding:5,
      alignContent:"center",
      alignItems:"center",
      alignSelf:"center",
      borderBottomLeftRadius: 10,
      borderBottomRightRadius: 10,
      borderTopLeftRadius: 10,
      borderTopRightRadius: 10,
      // alignItems: 'center',
      justifyContent: 'center',
    
    },
    inoutBoxPhone: {
      height:40,
      width:250,
      paddingTop:10,
      paddingBottom:10,
      paddingHorizontal:10,
      backgroundColor:'#fff',
      borderBottomWidth : 0.5,
      

    },
    forgot : {
      alignItems : 'flex-end',
      justifyContent : 'flex-end',
    },
    inputEmail: {
      height:40,
      paddingTop:10,
      paddingBottom:10,
      paddingHorizontal:10,
      backgroundColor:'#fff',
      marginTop:50
    },
    inputPassword: {
      height:40,
      paddingTop:10,
      paddingBottom:10,
      paddingHorizontal:10,
      backgroundColor:'#fff',
      marginBottom:10,
      marginTop:10
    },
    welcomeText:{
       alignItems: 'center',
      justifyContent: 'center',
      fontSize : 25,
      fontWeight : "bold",
      alignSelf: 'stretch',
      textAlign: 'center',
      color : '#00B7A5',
      paddingHorizontal:10
    },
    horizontalContainer1:{

      alignContent:'center',
      alignItems: 'center',
      justifyContent: 'center',
     flexDirection : "row"
    },

    horizontalContainer:{

  
      marginTop : 10,
      alignContent:"flex-start",
      alignItems:"flex-start",
      alignSelf:"flex-start",
     flexDirection : "row"
    },
    welcomeText1:{
   
     fontSize : 25,
     fontWeight : "bold",
 
     flex:1,
     color : '#000000',
     paddingTop : 50,
     paddingHorizontal:10
   },
   welcomeText2:{
   
    fontSize : 35,
    fontWeight : "bold",
  
    color : '#4FA647',
    paddingTop : 50,
    paddingHorizontal:10
  },


    subText:{
      marginBottom:40,
      alignSelf: 'stretch',
      fontSize:16,
      textAlign: 'center',
      paddingHorizontal:10,
      color : '#2E2E2E'
    
    },
    subText1:{
      alignContent : "flex-start",
      marginBottom:5,
      marginTop: 10,
      fontSize:16,
      color : '#101010',
      paddingHorizontal:10
    },
    ButtonContainer:{
      alignItems:'center',
      alignContent:"center",
      alignSelf:"center",
      marginTop : 40,
      padding:25,
      width:100,
      flexDirection: 'row',
      justifyContent: 'center',
      backgroundColor:'#4FA647',  
      paddingTop:15,
      paddingBottom:15,
      paddingHorizontal:10,
      marginBottom:10,
      borderRadius:25
    },
    setFontSize: {
     
      fontSize : 18,
     
    },
    inputBoxEmail:{
      alignSelf: 'stretch',
      height:40,
      paddingTop:5,
      paddingBottom:5,
      paddingHorizontal:10,
      marginTop:15,
      fontSize:16,
      borderWidth: 1,
      borderColor : '#eee',
      backgroundColor:'#fff',
      borderRadius:1
    },
    inputBoxPassword:{
      alignSelf: 'stretch',
      height:40,
      paddingTop:5,
      paddingBottom:5,
      paddingHorizontal:10,
      marginTop:10,
      fontSize:16,
      borderWidth: 1,
      borderColor : '#eee',
      backgroundColor:'#fff',
      borderRadius:1
    },
    imgmenu:{
  
      height : 250,
      width : width_proportion,
      marginTop:25,
    },
    header:{
      alignSelf: 'stretch',
      height : 100,
      backgroundColor : '#00B7A5',
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.5,
      shadowRadius: 2,
      elevation: 2,
    },
    setColorRed : {
      color: '#fff'
    }
  });
  