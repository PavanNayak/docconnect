import React from 'react';
import { AsyncStorage,Alert,Image,StyleSheet, Text, View, TextInput,TouchableOpacity,FlatList,ScrollView, KeyboardAvoidingView} from 'react-native';
import { ToastAndroid } from "react-native";
import axios from 'axios';
import Url from '../Utils/config';
import { StackActions, NavigationActions } from 'react-navigation';
const width_proportion = '100%';
var AuthStr='';

export default class DoctorDashboard extends React.Component{
  static navigationOptions = {
    header: null ,

  };


 

  constructor(props) {
    super(props);
    this.state = {
      email : '',
      password : '',
      catList: '',
    }
  }

  
  // doctorDetail(id,image){
  //   this.props.navigation.navigate("DoctorDetail",{'id':id,'profilePicUrl':image});
  // }

  
    render(){
      const {navigate} = this.props.navigation;
    return (
   
     <View style={styles.Maincontainer}>
     
      <View style={{flexDirection:"row",marginTop:100}}>

        <View style={{borderRadius:10,backgroundColor:"#3f51b5",flex:1,padding:20}}>
        <View>
        <View style={{backgroundColor:"#ffffff",borderRadius:25,width:50,height:50,padding:10}}>
        <Image resizeMode="contain" style={{height:20,width:20,alignContent:"center",alignItems:"center",alignSelf:"center",marginTop:5}} source={require('../assets/document.png')} />
        </View>
        <Text style={{fontSize:16,fontWeight:"bold",color:"#ffffff",marginTop:20}}>Appointments</Text>
        <Text style={{fontSize:12,color:"#ffffff",marginTop:1}}>List of appointments from patients</Text>
        </View>
        </View>

        <View style={{borderRadius:10,backgroundColor:"#66bb6a",flex:1,padding:20.,marginLeft:10}}>
        <View>
        <View style={{backgroundColor:"#ffffff",borderRadius:25,width:50,height:50,padding:10}}>
        <Image resizeMode="contain" style={{height:20,width:20,alignContent:"center",alignItems:"center",alignSelf:"center",marginTop:5}} source={require('../assets/document.png')} />
        </View>
        <Text style={{fontSize:16,fontWeight:"bold",color:"#ffffff",marginTop:20}}>Session fee</Text>
        <Text style={{fontSize:12,color:"#ffffff",marginTop:1}}>Update your session fee here</Text>
        </View>
        </View>
      </View>


      <View style={{flexDirection:"row",marginTop:10}}>

<View style={{borderRadius:10,backgroundColor:"#ffffff",flex:1,padding:20}}>
<View>
<View style={{backgroundColor:"#ef5350",borderRadius:25,width:50,height:50,padding:10}}>
<Image resizeMode="contain" style={{height:20,width:20,alignContent:"center",alignItems:"center",alignSelf:"center",marginTop:5,tintColor:"#ffffff"}} source={require('../assets/document.png')} />
</View>
<Text style={{fontSize:16,fontWeight:"bold",color:"#000000",marginTop:20}}>Social</Text>
<Text style={{fontSize:12,color:"#000000",marginTop:1}}>Chat, Call and Video call</Text>
</View>
</View>

<View style={{borderRadius:10,backgroundColor:"#ffffff",flex:1,padding:20.,marginLeft:10}}>
<View>
<View style={{backgroundColor:"#fbc02d",borderRadius:25,width:50,height:50,padding:10}}>
<Image resizeMode="contain" style={{height:20,width:20,alignContent:"center",alignItems:"center",alignSelf:"center",marginTop:5,tintColor:"#ffffff"}} source={require('../assets/document.png')} />
</View>
<Text style={{fontSize:16,fontWeight:"bold",color:"#000000",marginTop:20}}>Prescriptions</Text>
<Text style={{fontSize:12,color:"#000000",marginTop:1}}>Upload prescriptions</Text>
</View>
</View>
</View>
     
     </View>
     

    );
  }
}
  
  const styles = StyleSheet.create({
    Maincontainer: {
      
      padding:20,
      marginTop:10,
      flex: 1,
      
    },
    HeadingText : {
      color : "#000000",
      fontSize : 18,
      fontWeight : "bold",
      marginTop : 30,

    },
    textInputContainer: {
      marginBottom: 10,
      width:5,
    },
    container: {
     
      height:80,
      width:80,
      padding:5,
      alignContent:"center",
      alignItems:"center",
      alignSelf:"center",
      borderBottomLeftRadius: 10,
      borderBottomRightRadius: 10,
      borderTopLeftRadius: 10,
      borderTopRightRadius: 10,
      // alignItems: 'center',
      justifyContent: 'center',
    
    },
    inoutBoxPhone: {
      height:40,
      width:250,
      paddingTop:10,
      paddingBottom:10,
      paddingHorizontal:10,
      backgroundColor:'#fff',
      borderBottomWidth : 0.5,
      

    },
    forgot : {
      alignItems : 'flex-end',
      justifyContent : 'flex-end',
    },
    inputEmail: {
      height:40,
      paddingTop:10,
      paddingBottom:10,
      paddingHorizontal:10,
      backgroundColor:'#fff',
      marginTop:50
    },
    inputPassword: {
      height:40,
      paddingTop:10,
      paddingBottom:10,
      paddingHorizontal:10,
      backgroundColor:'#fff',
      marginBottom:10,
      marginTop:10
    },
    welcomeText:{
       alignItems: 'center',
      justifyContent: 'center',
      fontSize : 25,
      fontWeight : "bold",
      alignSelf: 'stretch',
      textAlign: 'center',
      color : '#00B7A5',
      paddingHorizontal:10
    },
    horizontalContainer1:{

      alignContent:'center',
      alignItems: 'center',
      justifyContent: 'center',
     flexDirection : "row"
    },

    horizontalContainer:{

  
      marginTop : 10,
      alignContent:"flex-start",
      alignItems:"flex-start",
      alignSelf:"flex-start",
     flexDirection : "row"
    },
    welcomeText1:{
   
     fontSize : 25,
     fontWeight : "bold",
 
     flex:1,
     color : '#000000',
     paddingTop : 50,
     paddingHorizontal:10
   },
   welcomeText2:{
   
    fontSize : 35,
    fontWeight : "bold",
  
    color : '#4FA647',
    paddingTop : 50,
    paddingHorizontal:10
  },


    subText:{
      marginBottom:40,
      alignSelf: 'stretch',
      fontSize:16,
      textAlign: 'center',
      paddingHorizontal:10,
      color : '#2E2E2E'
    
    },
    subText1:{
      alignContent : "flex-start",
      marginBottom:5,
      marginTop: 10,
      fontSize:16,
      color : '#101010',
      paddingHorizontal:10
    },
    ButtonContainer:{
      alignItems:'center',
      alignContent:"center",
      alignSelf:"center",
      marginTop : 40,
      padding:25,
      width:100,
      flexDirection: 'row',
      justifyContent: 'center',
      backgroundColor:'#4FA647',  
      paddingTop:15,
      paddingBottom:15,
      paddingHorizontal:10,
      marginBottom:10,
      borderRadius:25
    },
    setFontSize: {
     
      fontSize : 18,
     
    },
    inputBoxEmail:{
      alignSelf: 'stretch',
      height:40,
      paddingTop:5,
      paddingBottom:5,
      paddingHorizontal:10,
      marginTop:15,
      fontSize:16,
      borderWidth: 1,
      borderColor : '#eee',
      backgroundColor:'#fff',
      borderRadius:1
    },
    inputBoxPassword:{
      alignSelf: 'stretch',
      height:40,
      paddingTop:5,
      paddingBottom:5,
      paddingHorizontal:10,
      marginTop:10,
      fontSize:16,
      borderWidth: 1,
      borderColor : '#eee',
      backgroundColor:'#fff',
      borderRadius:1
    },
    imgmenu:{
  
      height : 250,
      width : width_proportion,
      marginTop:25,
    },
    header:{
      alignSelf: 'stretch',
      height : 100,
      backgroundColor : '#00B7A5',
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.5,
      shadowRadius: 2,
      elevation: 2,
    },
    setColorRed : {
      color: '#fff'
    }
  });
  