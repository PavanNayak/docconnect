import React from 'react';
import { Alert,Image,StyleSheet, Text, View, TextInput,TouchableOpacity,FlatList,ScrollView, KeyboardAvoidingView} from 'react-native';
import { ToastAndroid } from "react-native";
import axios from 'axios';
import Url from '../Utils/config';
import { StackActions, NavigationActions } from 'react-navigation';
const width_proportion = '100%';
import FileSystem from 'expo';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';
var role='';
export default class SignUp extends React.Component{
  static navigationOptions = {
    header: null ,

  };


 


  constructor(props) {
    super(props);
    this.state = {
      email : '',
      password : '',
      username:'',
      confirmPassword:'',
      type:'',
      step1:false,
      step2:false,
      docuri:'',
      profileuri:'',
      expertin:'',
      exp:'',
      contact:'',
      city:'',
      state:'',
      country:'',
      pincode:'',
      call:'',
      video:'',
      chat:'',
  
    }
  }


  continueUpload(){
    if(this.state.expertin.length==0){
      ToastAndroid.showWithGravityAndOffset(
        "Please enter your expert in",
  ToastAndroid.LONG,
  ToastAndroid.BOTTOM,
  25,
  50
  );
    }
    else  if(this.state.exp.length==0){
      ToastAndroid.showWithGravityAndOffset(
        "Please enter your experience",
  ToastAndroid.LONG,
  ToastAndroid.BOTTOM,
  25,
  50
  );
    }
    else  if(this.state.contact.length==0){
      ToastAndroid.showWithGravityAndOffset(
        "Please enter your contact number",
  ToastAndroid.LONG,
  ToastAndroid.BOTTOM,
  25,
  50
  );
    }
    else  if(this.state.city.length==0){
      ToastAndroid.showWithGravityAndOffset(
        "Please enter your city",
  ToastAndroid.LONG,
  ToastAndroid.BOTTOM,
  25,
  50
  );
    }
    else  if(this.state.state.length==0){
      ToastAndroid.showWithGravityAndOffset(
        "Please enter your state",
  ToastAndroid.LONG,
  ToastAndroid.BOTTOM,
  25,
  50
  );
    }
    else  if(this.state.country.length==0){
      ToastAndroid.showWithGravityAndOffset(
        "Please enter your country",
  ToastAndroid.LONG,
  ToastAndroid.BOTTOM,
  25,
  50
  );
    }
    else  if(this.state.pincode.length==0){
      ToastAndroid.showWithGravityAndOffset(
        "Please enter your pincode",
  ToastAndroid.LONG,
  ToastAndroid.BOTTOM,
  25,
  50
  );
    }
    else  if(this.state.docuri.length==0){
      ToastAndroid.showWithGravityAndOffset(
        "Please upload your document",
  ToastAndroid.LONG,
  ToastAndroid.BOTTOM,
  25,
  50
  );
    }
    else  if(this.state.profileuri.length==0){
      ToastAndroid.showWithGravityAndOffset(
        "Please upload your profile pic",
  ToastAndroid.LONG,
  ToastAndroid.BOTTOM,
  25,
  50
  );
    }
    else 
    {
      this.setState({step1:false})
      this.setState({step2:true})
    }
   
  }
  
  componentDidMount() {
   
   
    this.getPermissionAsync();


  }

  getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  
  };

  onClickType(type){
    this.setState({type:type})

  }


  registerSubmit(){
    if(this.state.chat.length==0 || this.state.call.length==0 || this.state.video.length==0){
      ToastAndroid.showWithGravityAndOffset(
        "Please enter all the fields",
  ToastAndroid.LONG,
  ToastAndroid.BOTTOM,
  25,
  50
  );
      }
  else
  {
    const formData = new FormData();
    formData.append( "userName",this.state.username);
    formData.append(  "email",this.state.email);
    formData.append( "password",this.state.password);
    formData.append( "confirmPassword",this.state.confirmPassword);
    formData.append( "expertIn",this.state.expertin);
    formData.append( "experience",this.state.exp);
    formData.append( "city",this.state.city);
    formData.append( "phoneNumber",this.state.contact);
    formData.append( "state",this.state.state);
    formData.append( "country",this.state.country);
    formData.append( "pincode",this.state.pincode);
    formData.append( "chat",this.state.chat);
    formData.append( "call",this.state.call);
    formData.append( "video",this.state.video);
    formData.append('documents', {
                         uri: this.state.docuri, //Your Image File Path
                        type: 'image/jpeg', 
                        name: "imagename.jpg",
                      });
                      formData.append('profilePic', {
                        uri: this.state.profileuri, //Your Image File Path
                       type: 'image/jpeg', 
                       name: "imagename.jpg",
                     });
    let self=this;

    axios({
      url: Url.BaseUrl + "user/doctorSignUp",
    method:'POST',
  data:formData})
   
  .then(function (responseJson) {
        console.log("Signup Response"+JSON.stringify(responseJson.data));
        if(responseJson.data.success==true){
          ToastAndroid.showWithGravityAndOffset(
            responseJson.data.message,
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      25,
      50
      );
          self.props.navigation.navigate("Login")
        }
        else 
        {
          ToastAndroid.showWithGravityAndOffset(
            "Something went wrong",
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      25,
      50
      );
        }
      })
      .catch((error) => {
        console.error(error);
      }); 

  }
    
  }

  _pickImage = async () => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.All,
        allowsEditing: true,
        aspect: [4, 3],
        quality: 1,
      });
      if (!result.cancelled) {

        // ImgToBase64.getBase64String('file://'+result.uri)
        // ImgToBase64.getBase64String(result.uri)
        // .then(base64String => doSomethingWith(base64String))
        
        // .catch(err => doSomethingWith(err));

        try {

          const file = result.uri;
          const uriParts = file.split('.');
          const fileType = uriParts[uriParts.length - 1];
          // const base64Img= await FileSystem.readAsStringAsync(result.uri, { encoding: 'base64' })
          // console.log("base64Img ::::::: "+base64Img);
          // this.setState({ image: result.uri});

          // const content = await FileSystem.readAsStringAsync(result.uri)
          console.log("FileSuccess "+fileType);
           console.log("FileSuccess2 "+uriParts);
              console.log("FileSuccess3 "+file);
          this.setState({docuri:file})

             
          
      } catch(e) {
          console.log('fileToBase64', e.message)
          return ''
      }
  

        
      }

      console.log(result);
    } catch (E) {
      console.log(E);
    }
  };


  _pickImage1 = async () => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.All,
        allowsEditing: true,
        aspect: [4, 3],
        quality: 1,
      });
      if (!result.cancelled) {

        // ImgToBase64.getBase64String('file://'+result.uri)
        // ImgToBase64.getBase64String(result.uri)
        // .then(base64String => doSomethingWith(base64String))
        
        // .catch(err => doSomethingWith(err));

        try {

          const file = result.uri;
          const uriParts = file.split('.');
          const fileType = uriParts[uriParts.length - 1];
          // const base64Img= await FileSystem.readAsStringAsync(result.uri, { encoding: 'base64' })
          // console.log("base64Img ::::::: "+base64Img);
          // this.setState({ image: result.uri});

          // const content = await FileSystem.readAsStringAsync(result.uri)
          console.log("FileSuccess "+fileType);
           console.log("FileSuccess2 "+uriParts);
              console.log("FileSuccess3 "+file);
          this.setState({profileuri:file})

             
          
      } catch(e) {
          console.log('fileToBase64', e.message)
          return ''
      }
  

        
      }

      console.log(result);
    } catch (E) {
      console.log(E);
    }
  };

  validateEmail = email => {
    var re = /^(([^<>()\[\]\\.,;:\s@”]+(\.[^<>()\[\]\\.,;:\s@”]+)*)|(“.+”))@((\[[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}])|(([a-zA-Z\-0–9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
    };


  loginPage(){
    
    this.props.navigation.navigate("Login");
  }

  continue(){
    if(this.state.type.length==0){
      ToastAndroid.showWithGravityAndOffset(
        "Please select the account type",
  ToastAndroid.LONG,
  ToastAndroid.BOTTOM,
  25,
  50
  );
    }
    else if(this.state.email.length==0){
      ToastAndroid.showWithGravityAndOffset(
        "Please enter Email id",
  ToastAndroid.LONG,
  ToastAndroid.BOTTOM,
  25,
  50
  );
    }
    else  if(this.state.password.length==0){
      ToastAndroid.showWithGravityAndOffset(
        "Please enter password",
  ToastAndroid.LONG,
  ToastAndroid.BOTTOM,
  25,
  50
  );
    }
    else  if(this.state.confirmPassword.length==0){
      ToastAndroid.showWithGravityAndOffset(
        "Please enter confirm password",
  ToastAndroid.LONG,
  ToastAndroid.BOTTOM,
  25,
  50
  );
    }
    else  if(this.state.username.length==0){
      ToastAndroid.showWithGravityAndOffset(
        "Please enter username",
  ToastAndroid.LONG,
  ToastAndroid.BOTTOM,
  25,
  50
  );
    }
    else  if (!this.validateEmail(this.state.email.trim())) {
                 
      ToastAndroid.showWithGravityAndOffset(
        "Invalid Email format!",
  ToastAndroid.LONG,
  ToastAndroid.BOTTOM,
  25,
  50
  );
      }
      else if(!(this.state.password==this.state.confirmPassword)){
             
        ToastAndroid.showWithGravityAndOffset(
          "Password doesnt match!",
    ToastAndroid.LONG,
    ToastAndroid.BOTTOM,
    25,
    50
    );
      }
      else
      { 
          this.setState({step1:true})
      }
  }
  homePage(){

    console.log("******"+this.state.type)
    if(this.state.type.length==0){
      ToastAndroid.showWithGravityAndOffset(
        "Please select the account type",
  ToastAndroid.LONG,
  ToastAndroid.BOTTOM,
  25,
  50
  );
    }
    else if(this.state.email.length==0){
      ToastAndroid.showWithGravityAndOffset(
        "Please enter Email id",
  ToastAndroid.LONG,
  ToastAndroid.BOTTOM,
  25,
  50
  );
    }
    else  if(this.state.password.length==0){
      ToastAndroid.showWithGravityAndOffset(
        "Please enter password",
  ToastAndroid.LONG,
  ToastAndroid.BOTTOM,
  25,
  50
  );
    }
    else  if(this.state.confirmPassword.length==0){
      ToastAndroid.showWithGravityAndOffset(
        "Please enter confirm password",
  ToastAndroid.LONG,
  ToastAndroid.BOTTOM,
  25,
  50
  );
    }
    else  if(this.state.username.length==0){
      ToastAndroid.showWithGravityAndOffset(
        "Please enter username",
  ToastAndroid.LONG,
  ToastAndroid.BOTTOM,
  25,
  50
  );
    }
    else  if (!this.validateEmail(this.state.email.trim())) {
                 
      ToastAndroid.showWithGravityAndOffset(
        "Invalid Email format!",
  ToastAndroid.LONG,
  ToastAndroid.BOTTOM,
  25,
  50
  );
      }
      else if(!(this.state.password==this.state.confirmPassword)){
             
        ToastAndroid.showWithGravityAndOffset(
          "Password doesnt match!",
    ToastAndroid.LONG,
    ToastAndroid.BOTTOM,
    25,
    50
    );
      }
      else
      { 
      
    let self=this;
 
    axios.post(Url.BaseUrl + "user/signup",{
      
        "userName" : this.state.username,
        "email" : this.state.email,
        "password" : this.state.password,
        "confirmPassword" : this.state.confirmPassword,
        "role" : "patient"
    }
  )
   
  .then(function (responseJson) {
        console.log("Signup Response"+JSON.stringify(responseJson.data));
       // self.setState({wishlist:responseJson.data});
        if(responseJson.data.success==false){
          ToastAndroid.showWithGravityAndOffset(
           responseJson.data.message,
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      25,
      50
      );
        }
        else
        {
         // console.log("Login Success************************");
          ToastAndroid.showWithGravityAndOffset(
           "Registered  Successfully!",
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      25,
      50
      );
      self.props.navigation.navigate("Dashbord");
        }
      
      })
      .catch((error) => {
        console.error(error);
      }); 
   // this.props.navigation.navigate("Dashbord");
  }
}

  
  
    render(){
      const {navigate} = this.props.navigation;
    return (
      <ScrollView>
     <View style={{flex:1,backgroundColor:"#ffffff"}}>
       <View style={{flex:1}}></View>
     <Image resizeMode="contain" style={{height:200,width:200,alignContent:"center",alignItems:"center",alignSelf:"center"}} source={require('../assets/logo_withwhitebg.png')} />

        {
          (() => { 
        if(this.state.step1 ==true){
          return(<View>
          <ScrollView>
          <View style={styles.Maincontainer}>
              
          <Text style={{fontSize:16,fontWeight:"bold",flex:1,marginBottom:20}}>Enter below fileds</Text>
          <TextInput 
          placeholder ="Expert In"
          placeholderTextColor="#666666"
          style={{ 
            padding:10,
            width:'100%',
            marginTop:10,
            borderColor:'#eeeeee',
            borderRadius:10,
            color:"#000000",
            backgroundColor:"#eeeeee",
            }}
            onChangeText={(text) => {
                this.setState({expertin:text})        
            }
          }
          />
           <TextInput 
           keyboardType={'numeric'}
          placeholder ="Experience"
          placeholderTextColor="#666666"
          style={{ 
            padding:10,
            width:'100%',
            marginTop:10,
            borderColor:'#eeeeee',
            borderRadius:10,
            color:"#000000",
            backgroundColor:"#eeeeee",
            }}
            onChangeText={(text) => {
                this.setState({exp:text})        
            }
          }
          />

<TextInput 
           keyboardType={'numeric'}
          placeholder ="Contact Number"
          placeholderTextColor="#666666"
          style={{ 
            padding:10,
            width:'100%',
            marginTop:10,
            borderColor:'#eeeeee',
            borderRadius:10,
            color:"#000000",
            backgroundColor:"#eeeeee",
            }}
            onChangeText={(text) => {
                this.setState({contact:text})        
            }
          }
          />

          <View style={{flexDirection:"row"}}>
            
          <TextInput 
          
          placeholder ="City"
          placeholderTextColor="#666666"
          style={{ 
            padding:10,
            flex:1,
            marginTop:10,
            borderColor:'#eeeeee',
            borderRadius:10,
            color:"#000000",
            backgroundColor:"#eeeeee",
            }}
            onChangeText={(text) => {
                this.setState({city:text})        
            }
          }
          />
           <TextInput 
          
          placeholder ="State"
          placeholderTextColor="#666666"
          style={{ 
            padding:10,
            flex:1,
            marginLeft:5,
            marginTop:10,
            borderColor:'#eeeeee',
            borderRadius:10,
            color:"#000000",
            backgroundColor:"#eeeeee",
            }}
            onChangeText={(text) => {
                this.setState({state:text})        
            }
          }
          />
          </View>

         
          <View style={{flexDirection:"row"}}>
            
          <TextInput 
          
          placeholder ="Country"
          placeholderTextColor="#666666"
          style={{ 
            padding:10,
            flex:1,
            marginTop:10,
            borderColor:'#eeeeee',
            borderRadius:10,
            color:"#000000",
            backgroundColor:"#eeeeee",
            }}
            onChangeText={(text) => {
                this.setState({country:text})        
            }
          }
          />
           <TextInput 
          keyboardType={'numeric'}
          placeholder ="Pincode"
          placeholderTextColor="#666666"
          style={{ 
            padding:10,
            flex:1,
            marginLeft:5,
            marginTop:10,
            borderColor:'#eeeeee',
            borderRadius:10,
            color:"#000000",
            backgroundColor:"#eeeeee",
            }}
            onChangeText={(text) => {
                this.setState({pincode:text})        
            }
          }
          />
          
          
       
          
          </View>
          <View style={{flexDirection:"row",marginTop:20}}>
          <TouchableOpacity onPress={()=>this._pickImage()} style={{flex:1}}>
          <View>
          {
            this.state.docuri.length==0 ?
            <Image resizeMode="contain" style={{height:40,width:40,alignContent:"center",alignItems:"center",alignSelf:"center",tintColor:"#666666"}} source={require('../assets/up-arrow.png')} />
            :
            <Image resizeMode="contain" style={{height:40,width:40,alignContent:"center",alignItems:"center",alignSelf:"center",tintColor:"#3f51b5"}} source={require('../assets/tick.png')} />
          }
          
          <Text style={{fontSize:16,color:"#666666",flex:1,marginTop:5,marginBottom:10,alignContent:"center",alignItems:"center",alignSelf:"center"}}>Upload Document</Text>
          </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={()=>this._pickImage1()} style={{flex:1}}>
          <View>
          {
            this.state.profileuri.length==0 ?
            <Image resizeMode="contain" style={{height:40,width:40,alignContent:"center",alignItems:"center",alignSelf:"center",tintColor:"#666666"}} source={require('../assets/up-arrow.png')} />
            :
            <Image resizeMode="contain" style={{height:40,width:40,alignContent:"center",alignItems:"center",alignSelf:"center",tintColor:"#3f51b5"}} source={require('../assets/tick.png')} />
          }
          <Text style={{fontSize:16,color:"#666666",flex:1,marginTop:5,marginBottom:10,alignContent:"center",alignItems:"center",alignSelf:"center"}}>Profile pic</Text>
          </View>
          </TouchableOpacity>
          </View>
          <TouchableOpacity onPress={this.continueUpload.bind(this)}><Text style={{fontSize:20,fontWeight:"bold",backgroundColor:"#3f51b5",padding:10,borderRadius:5,marginTop:20,width:'50%',textAlign:"center",color:"#ffffff"}}>Continue</Text></TouchableOpacity>

          <View style={{flex:1,marginBottom:200}}></View>
          </View>
          </ScrollView>
          </View>);

        }else if(this.state.step2 ==true){

          return(<ScrollView>
          <View style={styles.Maincontainer}>
              
          <Text style={{fontSize:16,fontWeight:"bold",flex:1,marginBottom:20}}>Enter your fees</Text>
          <TextInput 
          keyboardType={'numeric'}
          placeholder ="Chat"
          placeholderTextColor="#666666"
          style={{ 
            padding:10,
            width:'100%',
            marginTop:10,
            borderColor:'#eeeeee',
            borderRadius:10,
            color:"#000000",
            backgroundColor:"#eeeeee",
            }}
            onChangeText={(text) => {
                this.setState({chat:text})        
            }
          }/>
            <TextInput 
          keyboardType={'numeric'}
          placeholder ="Call"
          placeholderTextColor="#666666"
          style={{ 
            padding:10,
            width:'100%',
            marginTop:10,
            borderColor:'#eeeeee',
            borderRadius:10,
            color:"#000000",
            backgroundColor:"#eeeeee",
            }}
            onChangeText={(text) => {
                this.setState({call:text})        
            }
          }
          />

<TextInput 
          keyboardType={'numeric'}
          placeholder ="Video"
          placeholderTextColor="#666666"
          style={{ 
            padding:10,
            width:'100%',
            marginTop:10,
            borderColor:'#eeeeee',
            borderRadius:10,
            color:"#000000",
            backgroundColor:"#eeeeee",
            }}
            onChangeText={(text) => {
                this.setState({video:text})        
            }
          }
          />

<TouchableOpacity onPress={this.registerSubmit.bind(this)}><Text style={{fontSize:20,fontWeight:"bold",backgroundColor:"#3f51b5",padding:10,borderRadius:5,marginTop:20,width:'50%',textAlign:"center",color:"#ffffff"}}>Register</Text></TouchableOpacity>

<View style={{flex:1,marginBottom:500}}></View>

          </View>
          </ScrollView>
          )
        }
        else
        {
          return(
          <View style={styles.Maincontainer}>
        <Text style={{fontSize:25,fontWeight:"bold",backgroundColor:"#eeeeee",padding:10,borderRadius:5,marginTop:1,marginBottom:10}}>SignUp</Text>
       <Text style={{fontSize:16,fontWeight:"bold",flex:1}}>Choose account type</Text>
       
       <View style={{flexDirection:"row",marginTop:20,marginBottom:20}}>
       <TouchableOpacity onPress={()=>this.onClickType("patient")}>
      { this.state.type =='patient' ?
      <View style={{flex:1,borderWidth:1,borderColor:"#666",padding:10}}>
       <Image resizeMode="contain" style={{height:70,width:70,alignContent:"center",alignItems:"center",alignSelf:"center"}} source={require('../assets/doctor.png')} />
       <Text style={{fontSize:16,fontWeight:"bold",flex:1,alignContent:"center",alignItems:"center",alignSelf:"center"}}>Doctor</Text>

       </View>
       :

       <View style={{flex:1,borderWidth:0,borderColor:"#666",padding:10}}>
       <Image resizeMode="contain" style={{height:70,width:70,alignContent:"center",alignItems:"center",alignSelf:"center"}} source={require('../assets/doctor.png')} />
       <Text style={{fontSize:16,fontWeight:"bold",flex:1,alignContent:"center",alignItems:"center",alignSelf:"center"}}>Doctor</Text>

       </View>
      }
      
       </TouchableOpacity>
       <TouchableOpacity onPress={()=>this.onClickType("doctor")} style={{marginLeft:30}}>
       { this.state.type =='doctor' ?

       <View style={{flex:1,borderWidth:1,borderColor:"#666",padding:10}}>
       <Image resizeMode="contain" style={{height:70,width:70,alignContent:"center",alignItems:"center",alignSelf:"center"}} source={require('../assets/patient.png')} />
       <Text style={{fontSize:16,fontWeight:"bold",flex:1,alignContent:"center",alignItems:"center",alignSelf:"center"}}>Patient</Text>

       </View>
       :
       <View style={{flex:1,borderWidth:0,borderColor:"#666",padding:10}}>
       <Image resizeMode="contain" style={{height:70,width:70,alignContent:"center",alignItems:"center",alignSelf:"center"}} source={require('../assets/patient.png')} />
       <Text style={{fontSize:16,fontWeight:"bold",flex:1,alignContent:"center",alignItems:"center",alignSelf:"center"}}>Patient</Text>

       </View>
       }
       </TouchableOpacity>
       </View>
       <TextInput 
          placeholder ="Email"
          placeholderTextColor="#666666"
          style={{ 
            padding:10,
            width:'100%',
            marginTop:10,
            borderColor:'#eeeeee',
            borderRadius:10,
            color:"#000000",
            backgroundColor:"#eeeeee",
            }}
            onChangeText={(text) => {
                this.setState({email:text})        
            }
          }
          />
          <TextInput 
          placeholder ="Username"
          placeholderTextColor="#666666"
          style={{ 
            padding:10,
            width:'100%',
            marginTop:10,
            borderColor:'#eeeeee',
            borderRadius:10,
            color:"#000000",
            backgroundColor:"#eeeeee",
            }}
            onChangeText={(text) => {
              this.setState({username:text})        
            }
          }
          />
           <TextInput 
             secureTextEntry={true}
          placeholder ="Password"
          placeholderTextColor="#666666"
          style={{ 
            padding:10,
            width:'100%',
            marginTop:10,
            borderColor:'#eeeeee',
            borderRadius:10,
            color:"#000000",
            backgroundColor:"#eeeeee",
            }}
            onChangeText={(text) => {
              this.setState({password:text})     
            }
          }
          />
            <TextInput 
              secureTextEntry={true}
          placeholder ="Confirm Password"
          placeholderTextColor="#666666"
          style={{ 
            padding:10,
            width:'100%',
            marginTop:10,
            borderColor:'#eeeeee',
            borderRadius:10,
            color:"#000000",
            backgroundColor:"#eeeeee",
            }}
            onChangeText={(text) => {
              this.setState({confirmPassword:text})        
            }
          }
          />

          {this.state.type=='doctor' ?
          <TouchableOpacity onPress={this.homePage.bind(this)}><Text style={{fontSize:20,fontWeight:"bold",backgroundColor:"#3f51b5",padding:10,borderRadius:5,marginTop:10,width:'50%',textAlign:"center",color:"#ffffff"}}>Signup</Text></TouchableOpacity>
          :
          <TouchableOpacity onPress={this.continue.bind(this)}><Text style={{fontSize:20,fontWeight:"bold",backgroundColor:"#3f51b5",padding:10,borderRadius:5,marginTop:10,width:'50%',textAlign:"center",color:"#ffffff"}}>Continue</Text></TouchableOpacity>
          }

<View style={{width:'100%',backgroundColor:"#eeeeee",height:1,marginTop:20}}></View>
<Text style={{fontWeight:"bold",marginTop:15,width:'50%',textAlign:"center",color:"#666666"}}>Already have an account ?</Text>
<TouchableOpacity onPress={this.loginPage.bind(this)}><Text style={{fontSize:20,fontWeight:"bold",marginTop:5,width:'50%',textAlign:"center",color:"#3f51b5"}}>Login</Text></TouchableOpacity>
<View style={{flex:1,marginBottom:200}}></View></View>);
        }
        
        })() }
      
     </View>
    </ScrollView>
    );
  }
}
  
  const styles = StyleSheet.create({
    Maincontainer: {
      backgroundColor: '#ffffff',
      padding:20,
      marginTop:10,
      alignContent:"center",
      alignItems:"center",
      alignSelf:"center",
      flex: 1,
      width:'100%',
      
    },
    HeadingText : {
      color : "#000000",
      fontSize : 18,
      fontWeight : "bold",
      marginTop : 10,

    },
    textInputContainer: {
      marginBottom: 10,
      width:5,
    },
    container: {
     
      height:80,
      width:80,
      padding:5,
      alignContent:"center",
      alignItems:"center",
      alignSelf:"center",
      borderBottomLeftRadius: 10,
      borderBottomRightRadius: 10,
      borderTopLeftRadius: 10,
      borderTopRightRadius: 10,
      // alignItems: 'center',
      justifyContent: 'center',
    
    },
    inoutBoxPhone: {
      height:40,
      width:250,
      paddingTop:10,
      paddingBottom:10,
      paddingHorizontal:10,
      backgroundColor:'#fff',
      borderBottomWidth : 0.5,
      

    },
    forgot : {
      alignItems : 'flex-end',
      justifyContent : 'flex-end',
    },
    inputEmail: {
      height:40,
      paddingTop:10,
      paddingBottom:10,
      paddingHorizontal:10,
      backgroundColor:'#fff',
      marginTop:50
    },
    inputPassword: {
      height:40,
      paddingTop:10,
      paddingBottom:10,
      paddingHorizontal:10,
      backgroundColor:'#fff',
      marginBottom:10,
      marginTop:10
    },
    welcomeText:{
       alignItems: 'center',
      justifyContent: 'center',
      fontSize : 25,
      fontWeight : "bold",
      alignSelf: 'stretch',
      textAlign: 'center',
      color : '#00B7A5',
      paddingHorizontal:10
    },
    horizontalContainer1:{

      alignContent:'center',
      alignItems: 'center',
      justifyContent: 'center',
     flexDirection : "row"
    },

    horizontalContainer:{

  
      marginTop : 10,
      alignContent:"flex-start",
      alignItems:"flex-start",
      alignSelf:"flex-start",
     flexDirection : "row"
    },
    welcomeText1:{
   
     fontSize : 25,
     fontWeight : "bold",
 
     flex:1,
     color : '#000000',
     paddingTop : 50,
     paddingHorizontal:10
   },
   welcomeText2:{
   
    fontSize : 35,
    fontWeight : "bold",
  
    color : '#4FA647',
    paddingTop : 50,
    paddingHorizontal:10
  },


    subText:{
      marginBottom:40,
      alignSelf: 'stretch',
      fontSize:16,
      textAlign: 'center',
      paddingHorizontal:10,
      color : '#2E2E2E'
    
    },
    subText1:{
      alignContent : "flex-start",
      marginBottom:5,
      marginTop: 10,
      fontSize:16,
      color : '#101010',
      paddingHorizontal:10
    },
    ButtonContainer:{
      alignItems:'center',
      alignContent:"center",
      alignSelf:"center",
      marginTop : 40,
      padding:25,
      width:100,
      flexDirection: 'row',
      justifyContent: 'center',
      backgroundColor:'#4FA647',  
      paddingTop:15,
      paddingBottom:15,
      paddingHorizontal:10,
      marginBottom:10,
      borderRadius:25
    },
    setFontSize: {
     
      fontSize : 18,
     
    },
    inputBoxEmail:{
      alignSelf: 'stretch',
      height:40,
      paddingTop:5,
      paddingBottom:5,
      paddingHorizontal:10,
      marginTop:15,
      fontSize:16,
      borderWidth: 1,
      borderColor : '#eee',
      backgroundColor:'#fff',
      borderRadius:1
    },
    inputBoxPassword:{
      alignSelf: 'stretch',
      height:40,
      paddingTop:5,
      paddingBottom:5,
      paddingHorizontal:10,
      marginTop:10,
      fontSize:16,
      borderWidth: 1,
      borderColor : '#eee',
      backgroundColor:'#fff',
      borderRadius:1
    },
    imgmenu:{
  
      height : 250,
      width : width_proportion,
      marginTop:25,
    },
    header:{
      alignSelf: 'stretch',
      height : 100,
      backgroundColor : '#00B7A5',
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.5,
      shadowRadius: 2,
      elevation: 2,
    },
    setColorRed : {
      color: '#fff'
    }
  });
  