import React from 'react';
import { Alert,Image,StyleSheet, Text, View, TextInput,TouchableOpacity,AsyncStorage,ScrollView, KeyboardAvoidingView} from 'react-native';
import { ToastAndroid } from "react-native";
import { StackActions, NavigationActions } from 'react-navigation';
const width_proportion = '100%';
const height_proportion = '50%';
import AppIntroSlider from 'react-native-app-intro-slider';
const goToHome = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'HomeDrawer' })],
});
const goToHome1 = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'HomeDrawerAfterLogin' })],
});


const slides = [
  {
    key: 'one',
    title: 'Welcome',
    text: 'Taking care of your health has become easire! Learn more, how to do it.',
    image: require('../assets/splash1.png'),
    backgroundColor: '#59b2ab',
  },
  {
    key: 'two',
    title: 'Get rid of the queues',
    text: 'Select the required specialist and make an appointment through the application ',
    image: require('../assets/splash2.png'),
    backgroundColor: '#febe29',
  },
  {
    key: 'three',
    title: 'Be in touch!',
    text: 'Turn on notification and we will notify you about your upcoming events in your calendar',
    image: require('../assets/splash3.png'),
    backgroundColor: '#22bcb5',
  }
];



export default class Introduction extends React.Component{
  static navigationOptions = {
    header: null ,

  };

  constructor(props) {
    super(props);
    this.state = {
    
   
        showRealApp: false
   
  
    }
  }
  
  async setIntroFlag(){
    await AsyncStorage.setItem('introFlag',"1");

    if(this.getLoginStatus() || this.getLoginStatus()=='true'){
      
      this.props.navigation.dispatch(goToHome1);
    }
    else
    this.props.navigation.dispatch(goToHome);
  
  }
  
  async getLoginStatus(){
    var flagValue=await AsyncStorage.getItem('isLogin');
    console.log("FlagValue : "+flagValue);
    return await AsyncStorage.getItem('isLogin');
  }
  async getIntroFlag(){
    return await AsyncStorage.getItem('introFlag')
  }


  _renderItem = ({ item }) => {
   
    return (
      <View style={{flex:1,justifyContent:"center",alignContent:"center",alignItems:"center",alignSelf:"center"}}>
       
        <Image resizeMode="contain" source={item.image} />
        <View style={{position:"absolute",bottom:100,marginLeft:50,marginRight:50}}>
           <Text style={{color:"#000000",fontWeight:"bold",fontSize:25,textAlign:"center"}}>{item.title}</Text>
         <Text style={{color:"#666666",textAlign:"center"}}>{item.text}</Text>
          </View>
      

      </View>
    );
  }
  _onDone = () => {
    // User finished the introduction. Show real app through
    // navigation or simply by controlling state
    // this.setState({ showRealApp: true });
    // this.props.navigation.navigate("Login");

    this.props.navigation.navigate("Login");
    

  }
  render() {
    console.log("IntroFlag "+this.getIntroFlag());
    if(this.getIntroFlag()=="1" || this.getIntroFlag()==1){
      this.setIntroFlag();
    }else{
      return (
        <View style={styles.Maincontainer}>
      <AppIntroSlider renderItem={this._renderItem} data={slides} onDone={this._onDone}/>
      </View>
      );
    }
  }
}
  
  const styles = StyleSheet.create({
    Maincontainer: {
    
      // alignItems: 'center',
     
      // justifyContent: 'center',
      flex: 1,
      
    }
  });
  