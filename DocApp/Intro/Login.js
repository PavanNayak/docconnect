import React from 'react';
import { Alert,Image,StyleSheet, Text, View, TextInput,TouchableOpacity,FlatList,ScrollView, KeyboardAvoidingView,AsyncStorage} from 'react-native';
import { ToastAndroid } from "react-native";

import { StackActions, NavigationActions } from 'react-navigation';
const width_proportion = '100%';
import axios from 'axios';
import Url from '../Utils/config';
export default class Login extends React.Component{
  static navigationOptions = {
    header: null ,

  };


 

  constructor(props) {
    super(props);
    this.state = {
      email : '',
      password : '',
  
    }
  }

  validateEmail = email => {
    var re = /^(([^<>()\[\]\\.,;:\s@”]+(\.[^<>()\[\]\\.,;:\s@”]+)*)|(“.+”))@((\[[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}])|(([a-zA-Z\-0–9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
    };


  registerPage(){
    
    this.props.navigation.navigate("SignUp");
  }
  async homePage(){

    if(this.state.email.length==0){
      ToastAndroid.showWithGravityAndOffset(
        "Please enter Email id",
  ToastAndroid.LONG,
  ToastAndroid.BOTTOM,
  25,
  50
  );
    }
    else  if(this.state.password.length==0){
      ToastAndroid.showWithGravityAndOffset(
        "Please enter password",
  ToastAndroid.LONG,
  ToastAndroid.BOTTOM,
  25,
  50
  );
    }
    else  if (!this.validateEmail(this.state.email.trim())) {
                 
      ToastAndroid.showWithGravityAndOffset(
        "Invalid Email format!",
  ToastAndroid.LONG,
  ToastAndroid.BOTTOM,
  25,
  50
  );
      }
      else
      { 

    let self=this;
 
    axios.post(Url.BaseUrl + "user/login",{
      "email" : this.state.email,
      "password" : this.state.password
  })
   
  .then(function (responseJson) {
        console.log("Login Response"+JSON.stringify(responseJson.data));
       // self.setState({wishlist:responseJson.data});
        if(responseJson.data.success==false){
          ToastAndroid.showWithGravityAndOffset(
           responseJson.data.error,
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      25,
      50
      );
        }
        else
        {
          if(responseJson.data.role=='patient'){
            ToastAndroid.showWithGravityAndOffset(
              "Logged in Successfully!",
         ToastAndroid.LONG,
         ToastAndroid.BOTTOM,
         25,
         50
         );
   
         AsyncStorage.setItem('token',responseJson.data.token).then(()=>
         self.props.navigation.navigate("Dashbord")
         );
          }
          else
          {
            ToastAndroid.showWithGravityAndOffset(
              "Logged in Successfully!",
         ToastAndroid.LONG,
         ToastAndroid.BOTTOM,
         25,
         50
         );
         AsyncStorage.setItem('user_id',responseJson.data.id).then(()=>
         {
         AsyncStorage.setItem('token',responseJson.data.token).then(()=>
         self.props.navigation.navigate("DoctorDashboard")
         );
         }
         );
            
          }
         // console.log("Login Success************************");
         
     
        }
      
      })
      .catch((error) => {
        console.error(error);
      }); 
   // this.props.navigation.navigate("Dashbord");
  }
}

  
    render(){
      const {navigate} = this.props.navigation;
    return (
   <ScrollView>
     <View style={styles.Maincontainer}>
       <View style={{flex:1}}></View>
     <Image resizeMode="contain" style={{height:250,width:250,alignContent:"center",alignItems:"center",alignSelf:"center"}} source={require('../assets/logo_withwhitebg.png')} />
       <Text style={{fontSize:25,fontWeight:"bold",backgroundColor:"#eeeeee",padding:10,borderRadius:5,marginTop:30,marginBottom:10}}>Login</Text>

       <TextInput 
          placeholder ="Username or Email"
          placeholderTextColor="#666666"
          style={{ 
            padding:10,
            width:'100%',
            marginTop:10,
            borderColor:'#eeeeee',
            borderRadius:10,
            color:"#000000",
            backgroundColor:"#eeeeee",
            }}
            onChangeText={(text) => {
                        this.setState({email:text})
            }
          }
          />
          <TextInput 
            secureTextEntry={true}
          placeholder ="Password"
          placeholderTextColor="#666666"
          style={{ 
            padding:10,
            width:'100%',
            marginTop:10,
            borderColor:'#eeeeee',
            borderRadius:10,
            color:"#000000",
            backgroundColor:"#eeeeee",
            }}
            onChangeText={(text) => {
              this.setState({password:text})  
            }
          }
          />
 <TouchableOpacity onPress={this.homePage.bind(this)}><Text style={{fontSize:20,fontWeight:"bold",backgroundColor:"#3f51b5",padding:10,borderRadius:5,marginTop:10,width:'50%',textAlign:"center",color:"#ffffff"}}>      Let me in      </Text></TouchableOpacity>
<View style={{width:'100%',backgroundColor:"#eeeeee",height:1,marginTop:20}}></View>
<Text style={{fontWeight:"bold",marginTop:15,width:'50%',textAlign:"center",color:"#666666"}}>Don't have an account ?</Text>
<TouchableOpacity onPress={this.registerPage.bind(this)}><Text style={{fontSize:20,fontWeight:"bold",marginTop:5,width:'50%',textAlign:"center",color:"#3f51b5"}}>Sign Up</Text></TouchableOpacity>
<View style={{flex:1,marginBottom:200}}></View>
     </View>
     </ScrollView>
    );
  }
}
  

  const styles = StyleSheet.create({
    Maincontainer: {
      backgroundColor: '#ffffff',
      padding:20,
      marginTop:10,
      alignContent:"center",
      alignItems:"center",
      alignSelf:"center",
      flex: 1,
      width:'100%',
      
    },
    HeadingText : {
      color : "#000000",
      fontSize : 18,
      fontWeight : "bold",
      marginTop : 10,

    },
    textInputContainer: {
      marginBottom: 10,
      width:5,
    },
    container: {
     
      height:80,
      width:80,
      padding:5,
      alignContent:"center",
      alignItems:"center",
      alignSelf:"center",
      borderBottomLeftRadius: 10,
      borderBottomRightRadius: 10,
      borderTopLeftRadius: 10,
      borderTopRightRadius: 10,
      // alignItems: 'center',
      justifyContent: 'center',
    
    },
    inoutBoxPhone: {
      height:40,
      width:250,
      paddingTop:10,
      paddingBottom:10,
      paddingHorizontal:10,
      backgroundColor:'#fff',
      borderBottomWidth : 0.5,
      

    },
    forgot : {
      alignItems : 'flex-end',
      justifyContent : 'flex-end',
    },
    inputEmail: {
      height:40,
      paddingTop:10,
      paddingBottom:10,
      paddingHorizontal:10,
      backgroundColor:'#fff',
      marginTop:50
    },
    inputPassword: {
      height:40,
      paddingTop:10,
      paddingBottom:10,
      paddingHorizontal:10,
      backgroundColor:'#fff',
      marginBottom:10,
      marginTop:10
    },
    welcomeText:{
       alignItems: 'center',
      justifyContent: 'center',
      fontSize : 25,
      fontWeight : "bold",
      alignSelf: 'stretch',
      textAlign: 'center',
      color : '#00B7A5',
      paddingHorizontal:10
    },
    horizontalContainer1:{

      alignContent:'center',
      alignItems: 'center',
      justifyContent: 'center',
     flexDirection : "row"
    },

    horizontalContainer:{

  
      marginTop : 10,
      alignContent:"flex-start",
      alignItems:"flex-start",
      alignSelf:"flex-start",
     flexDirection : "row"
    },
    welcomeText1:{
   
     fontSize : 25,
     fontWeight : "bold",
 
     flex:1,
     color : '#000000',
     paddingTop : 50,
     paddingHorizontal:10
   },
   welcomeText2:{
   
    fontSize : 35,
    fontWeight : "bold",
  
    color : '#4FA647',
    paddingTop : 50,
    paddingHorizontal:10
  },


    subText:{
      marginBottom:40,
      alignSelf: 'stretch',
      fontSize:16,
      textAlign: 'center',
      paddingHorizontal:10,
      color : '#2E2E2E'
    
    },
    subText1:{
      alignContent : "flex-start",
      marginBottom:5,
      marginTop: 10,
      fontSize:16,
      color : '#101010',
      paddingHorizontal:10
    },
    ButtonContainer:{
      alignItems:'center',
      alignContent:"center",
      alignSelf:"center",
      marginTop : 40,
      padding:25,
      width:100,
      flexDirection: 'row',
      justifyContent: 'center',
      backgroundColor:'#4FA647',  
      paddingTop:15,
      paddingBottom:15,
      paddingHorizontal:10,
      marginBottom:10,
      borderRadius:25
    },
    setFontSize: {
     
      fontSize : 18,
     
    },
    inputBoxEmail:{
      alignSelf: 'stretch',
      height:40,
      paddingTop:5,
      paddingBottom:5,
      paddingHorizontal:10,
      marginTop:15,
      fontSize:16,
      borderWidth: 1,
      borderColor : '#eee',
      backgroundColor:'#fff',
      borderRadius:1
    },
    inputBoxPassword:{
      alignSelf: 'stretch',
      height:40,
      paddingTop:5,
      paddingBottom:5,
      paddingHorizontal:10,
      marginTop:10,
      fontSize:16,
      borderWidth: 1,
      borderColor : '#eee',
      backgroundColor:'#fff',
      borderRadius:1
    },
    imgmenu:{
  
      height : 250,
      width : width_proportion,
      marginTop:25,
    },
    header:{
      alignSelf: 'stretch',
      height : 100,
      backgroundColor : '#00B7A5',
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.5,
      shadowRadius: 2,
      elevation: 2,
    },
    setColorRed : {
      color: '#fff'
    }
  });
  